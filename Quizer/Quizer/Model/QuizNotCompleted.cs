﻿using System;
using System.Runtime.Serialization;

namespace Quizer
{
    [Serializable]
    internal class QuizNotCompleted : Exception
    {
        public QuizNotCompleted()
        {
        }

        public QuizNotCompleted(string message) : base(message)
        {
        }

        public QuizNotCompleted(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected QuizNotCompleted(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}