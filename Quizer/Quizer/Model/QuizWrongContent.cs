﻿using System;
using System.Runtime.Serialization;

namespace Quizer
{
    [Serializable]
    internal class QuizWrongContent : Exception
    {
        public QuizWrongContent()
        {
        }

        public QuizWrongContent(string message) : base(message)
        {
        }

        public QuizWrongContent(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected QuizWrongContent(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}