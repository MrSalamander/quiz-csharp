﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using QuizLib;

namespace Quizer
{
    class Model
    {
        public string openFile(string file)
        {
            string getFile = Path.Combine(file.ToString());
            FileInfo fi = new FileInfo(getFile);
            string acquiredJson = "";
            try
            {
                using (StreamReader sr = new StreamReader(fi.FullName))
                {
                    String line = sr.ReadToEnd();
                    acquiredJson += line;
                }
            }
            catch(IOException er)
            {
                throw er;
            }
            return acquiredJson;
        }
        public Quiz getQuizFromJson(string path)
        {
            Quiz quiz;
            string json = "";

            json = this.openFile(path);
            quiz = JsonConvert.DeserializeObject<Quiz>(json);
            if (quiz.Descryption.ToString() == null || quiz.Owner == null || quiz.Title == null || quiz.Version == null || quiz.Questions.Count < 1)
            {
                throw new Exception("Error - ");
            }
            return quiz;
        }
    }
}
