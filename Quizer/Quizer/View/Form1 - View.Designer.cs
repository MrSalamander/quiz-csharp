﻿namespace Quizer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_LoadQuiz = new System.Windows.Forms.GroupBox();
            this.button_LoadFile = new System.Windows.Forms.Button();
            this.label_hello = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label_Question = new System.Windows.Forms.Label();
            this.groupBox_Question = new System.Windows.Forms.GroupBox();
            this.button_EndQuiz = new System.Windows.Forms.Button();
            this.listBox_Answers = new System.Windows.Forms.ListBox();
            this.button_Next = new System.Windows.Forms.Button();
            this.button_Prev = new System.Windows.Forms.Button();
            this.groupBox_Info = new System.Windows.Forms.GroupBox();
            this.button_Start = new System.Windows.Forms.Button();
            this.label_QuizOwner_Content = new System.Windows.Forms.Label();
            this.label_QuizVersion_Content = new System.Windows.Forms.Label();
            this.label_QuizDesc_Content = new System.Windows.Forms.Label();
            this.label_QuizName_Content = new System.Windows.Forms.Label();
            this.label_QuizOwner = new System.Windows.Forms.Label();
            this.label_QuizVersion = new System.Windows.Forms.Label();
            this.label_QuizDesc = new System.Windows.Forms.Label();
            this.label_QuizName = new System.Windows.Forms.Label();
            this.groupBox_Summary = new System.Windows.Forms.GroupBox();
            this.label_Qscore = new System.Windows.Forms.Label();
            this.label_Qname = new System.Windows.Forms.Label();
            this.label_QscoreText = new System.Windows.Forms.Label();
            this.button_EndProgram = new System.Windows.Forms.Button();
            this.button_Check = new System.Windows.Forms.Button();
            this.label_QnameText = new System.Windows.Forms.Label();
            this.groupBox_LoadQuiz.SuspendLayout();
            this.groupBox_Question.SuspendLayout();
            this.groupBox_Info.SuspendLayout();
            this.groupBox_Summary.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_LoadQuiz
            // 
            this.groupBox_LoadQuiz.Controls.Add(this.button_LoadFile);
            this.groupBox_LoadQuiz.Controls.Add(this.label_hello);
            this.groupBox_LoadQuiz.Location = new System.Drawing.Point(12, 12);
            this.groupBox_LoadQuiz.Name = "groupBox_LoadQuiz";
            this.groupBox_LoadQuiz.Size = new System.Drawing.Size(406, 258);
            this.groupBox_LoadQuiz.TabIndex = 5;
            this.groupBox_LoadQuiz.TabStop = false;
            this.groupBox_LoadQuiz.Text = "Wczytanie quizu";
            // 
            // button_LoadFile
            // 
            this.button_LoadFile.Location = new System.Drawing.Point(114, 179);
            this.button_LoadFile.Name = "button_LoadFile";
            this.button_LoadFile.Size = new System.Drawing.Size(177, 57);
            this.button_LoadFile.TabIndex = 1;
            this.button_LoadFile.Text = "button1";
            this.button_LoadFile.UseVisualStyleBackColor = true;
            this.button_LoadFile.Click += new System.EventHandler(this.button_LoadFile_Click);
            // 
            // label_hello
            // 
            this.label_hello.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_hello.Location = new System.Drawing.Point(35, 50);
            this.label_hello.Name = "label_hello";
            this.label_hello.Size = new System.Drawing.Size(325, 112);
            this.label_hello.TabIndex = 0;
            this.label_hello.Text = "a";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label_Question
            // 
            this.label_Question.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_Question.Location = new System.Drawing.Point(24, 32);
            this.label_Question.Name = "label_Question";
            this.label_Question.Size = new System.Drawing.Size(361, 42);
            this.label_Question.TabIndex = 0;
            this.label_Question.Text = "label_Qusetion";
            // 
            // groupBox_Question
            // 
            this.groupBox_Question.Controls.Add(this.button_EndQuiz);
            this.groupBox_Question.Controls.Add(this.listBox_Answers);
            this.groupBox_Question.Controls.Add(this.button_Next);
            this.groupBox_Question.Controls.Add(this.button_Prev);
            this.groupBox_Question.Controls.Add(this.label_Question);
            this.groupBox_Question.Location = new System.Drawing.Point(0, 0);
            this.groupBox_Question.Name = "groupBox_Question";
            this.groupBox_Question.Size = new System.Drawing.Size(406, 258);
            this.groupBox_Question.TabIndex = 2;
            this.groupBox_Question.TabStop = false;
            this.groupBox_Question.Text = "Pytanie";
            // 
            // button_EndQuiz
            // 
            this.button_EndQuiz.Enabled = false;
            this.button_EndQuiz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_EndQuiz.Location = new System.Drawing.Point(326, 219);
            this.button_EndQuiz.Name = "button_EndQuiz";
            this.button_EndQuiz.Size = new System.Drawing.Size(74, 33);
            this.button_EndQuiz.TabIndex = 4;
            this.button_EndQuiz.Text = "buttonEnd";
            this.button_EndQuiz.UseVisualStyleBackColor = true;
            this.button_EndQuiz.Visible = false;
            this.button_EndQuiz.Click += new System.EventHandler(this.buttonEnd_Click);
            // 
            // listBox_Answers
            // 
            this.listBox_Answers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listBox_Answers.FormattingEnabled = true;
            this.listBox_Answers.ItemHeight = 15;
            this.listBox_Answers.Location = new System.Drawing.Point(24, 77);
            this.listBox_Answers.Name = "listBox_Answers";
            this.listBox_Answers.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Answers.Size = new System.Drawing.Size(361, 124);
            this.listBox_Answers.TabIndex = 3;
            // 
            // button_Next
            // 
            this.button_Next.Enabled = false;
            this.button_Next.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_Next.Location = new System.Drawing.Point(326, 219);
            this.button_Next.Name = "button_Next";
            this.button_Next.Size = new System.Drawing.Size(74, 33);
            this.button_Next.TabIndex = 2;
            this.button_Next.Text = "button2";
            this.button_Next.UseVisualStyleBackColor = true;
            this.button_Next.Click += new System.EventHandler(this.button_Next_Click_1);
            // 
            // button_Prev
            // 
            this.button_Prev.Enabled = false;
            this.button_Prev.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_Prev.Location = new System.Drawing.Point(6, 219);
            this.button_Prev.Name = "button_Prev";
            this.button_Prev.Size = new System.Drawing.Size(77, 33);
            this.button_Prev.TabIndex = 1;
            this.button_Prev.Text = "button1";
            this.button_Prev.UseVisualStyleBackColor = true;
            this.button_Prev.Click += new System.EventHandler(this.button_Prev_Click);
            // 
            // groupBox_Info
            // 
            this.groupBox_Info.Controls.Add(this.button_Start);
            this.groupBox_Info.Controls.Add(this.label_QuizOwner_Content);
            this.groupBox_Info.Controls.Add(this.label_QuizVersion_Content);
            this.groupBox_Info.Controls.Add(this.label_QuizDesc_Content);
            this.groupBox_Info.Controls.Add(this.label_QuizName_Content);
            this.groupBox_Info.Controls.Add(this.label_QuizOwner);
            this.groupBox_Info.Controls.Add(this.label_QuizVersion);
            this.groupBox_Info.Controls.Add(this.label_QuizDesc);
            this.groupBox_Info.Controls.Add(this.label_QuizName);
            this.groupBox_Info.Location = new System.Drawing.Point(0, 0);
            this.groupBox_Info.Name = "groupBox_Info";
            this.groupBox_Info.Size = new System.Drawing.Size(406, 258);
            this.groupBox_Info.TabIndex = 3;
            this.groupBox_Info.TabStop = false;
            this.groupBox_Info.Text = "Pytanie";
            this.groupBox_Info.Visible = false;
            // 
            // button_Start
            // 
            this.button_Start.Location = new System.Drawing.Point(282, 206);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(104, 46);
            this.button_Start.TabIndex = 8;
            this.button_Start.Text = "button1";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // label_QuizOwner_Content
            // 
            this.label_QuizOwner_Content.AutoSize = true;
            this.label_QuizOwner_Content.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_QuizOwner_Content.Location = new System.Drawing.Point(122, 174);
            this.label_QuizOwner_Content.Name = "label_QuizOwner_Content";
            this.label_QuizOwner_Content.Size = new System.Drawing.Size(51, 20);
            this.label_QuizOwner_Content.TabIndex = 7;
            this.label_QuizOwner_Content.Text = "label1";
            // 
            // label_QuizVersion_Content
            // 
            this.label_QuizVersion_Content.AutoSize = true;
            this.label_QuizVersion_Content.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_QuizVersion_Content.Location = new System.Drawing.Point(122, 130);
            this.label_QuizVersion_Content.Name = "label_QuizVersion_Content";
            this.label_QuizVersion_Content.Size = new System.Drawing.Size(51, 20);
            this.label_QuizVersion_Content.TabIndex = 6;
            this.label_QuizVersion_Content.Text = "label1";
            // 
            // label_QuizDesc_Content
            // 
            this.label_QuizDesc_Content.AutoSize = true;
            this.label_QuizDesc_Content.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_QuizDesc_Content.Location = new System.Drawing.Point(122, 86);
            this.label_QuizDesc_Content.Name = "label_QuizDesc_Content";
            this.label_QuizDesc_Content.Size = new System.Drawing.Size(51, 20);
            this.label_QuizDesc_Content.TabIndex = 5;
            this.label_QuizDesc_Content.Text = "label1";
            // 
            // label_QuizName_Content
            // 
            this.label_QuizName_Content.AutoSize = true;
            this.label_QuizName_Content.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_QuizName_Content.Location = new System.Drawing.Point(122, 42);
            this.label_QuizName_Content.Name = "label_QuizName_Content";
            this.label_QuizName_Content.Size = new System.Drawing.Size(51, 20);
            this.label_QuizName_Content.TabIndex = 4;
            this.label_QuizName_Content.Text = "label1";
            // 
            // label_QuizOwner
            // 
            this.label_QuizOwner.AutoSize = true;
            this.label_QuizOwner.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_QuizOwner.Location = new System.Drawing.Point(26, 174);
            this.label_QuizOwner.Name = "label_QuizOwner";
            this.label_QuizOwner.Size = new System.Drawing.Size(51, 20);
            this.label_QuizOwner.TabIndex = 3;
            this.label_QuizOwner.Text = "label1";
            // 
            // label_QuizVersion
            // 
            this.label_QuizVersion.AutoSize = true;
            this.label_QuizVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_QuizVersion.Location = new System.Drawing.Point(26, 130);
            this.label_QuizVersion.Name = "label_QuizVersion";
            this.label_QuizVersion.Size = new System.Drawing.Size(51, 20);
            this.label_QuizVersion.TabIndex = 2;
            this.label_QuizVersion.Text = "label1";
            // 
            // label_QuizDesc
            // 
            this.label_QuizDesc.AutoSize = true;
            this.label_QuizDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_QuizDesc.Location = new System.Drawing.Point(26, 86);
            this.label_QuizDesc.Name = "label_QuizDesc";
            this.label_QuizDesc.Size = new System.Drawing.Size(51, 20);
            this.label_QuizDesc.TabIndex = 1;
            this.label_QuizDesc.Text = "label1";
            // 
            // label_QuizName
            // 
            this.label_QuizName.AutoSize = true;
            this.label_QuizName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_QuizName.Location = new System.Drawing.Point(26, 42);
            this.label_QuizName.Name = "label_QuizName";
            this.label_QuizName.Size = new System.Drawing.Size(51, 20);
            this.label_QuizName.TabIndex = 0;
            this.label_QuizName.Text = "label1";
            // 
            // groupBox_Summary
            // 
            this.groupBox_Summary.Controls.Add(this.label_Qscore);
            this.groupBox_Summary.Controls.Add(this.label_Qname);
            this.groupBox_Summary.Controls.Add(this.label_QscoreText);
            this.groupBox_Summary.Controls.Add(this.button_EndProgram);
            this.groupBox_Summary.Controls.Add(this.button_Check);
            this.groupBox_Summary.Controls.Add(this.label_QnameText);
            this.groupBox_Summary.Location = new System.Drawing.Point(0, 0);
            this.groupBox_Summary.Name = "groupBox_Summary";
            this.groupBox_Summary.Size = new System.Drawing.Size(418, 270);
            this.groupBox_Summary.TabIndex = 4;
            this.groupBox_Summary.TabStop = false;
            this.groupBox_Summary.Text = "Pytanie";
            this.groupBox_Summary.Enter += new System.EventHandler(this.groupBox_Summary_Enter);
            // 
            // label_Qscore
            // 
            this.label_Qscore.AutoSize = true;
            this.label_Qscore.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_Qscore.Location = new System.Drawing.Point(200, 89);
            this.label_Qscore.Name = "label_Qscore";
            this.label_Qscore.Size = new System.Drawing.Size(103, 17);
            this.label_Qscore.TabIndex = 5;
            this.label_Qscore.Text = "label_Qusetion";
            // 
            // label_Qname
            // 
            this.label_Qname.AutoSize = true;
            this.label_Qname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_Qname.Location = new System.Drawing.Point(200, 32);
            this.label_Qname.Name = "label_Qname";
            this.label_Qname.Size = new System.Drawing.Size(103, 17);
            this.label_Qname.TabIndex = 4;
            this.label_Qname.Text = "label_Qusetion";
            // 
            // label_QscoreText
            // 
            this.label_QscoreText.AutoSize = true;
            this.label_QscoreText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_QscoreText.Location = new System.Drawing.Point(24, 86);
            this.label_QscoreText.Name = "label_QscoreText";
            this.label_QscoreText.Size = new System.Drawing.Size(119, 17);
            this.label_QscoreText.TabIndex = 3;
            this.label_QscoreText.Text = "label_QscoreText";
            // 
            // button_EndProgram
            // 
            this.button_EndProgram.Enabled = false;
            this.button_EndProgram.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_EndProgram.Location = new System.Drawing.Point(326, 219);
            this.button_EndProgram.Name = "button_EndProgram";
            this.button_EndProgram.Size = new System.Drawing.Size(74, 33);
            this.button_EndProgram.TabIndex = 2;
            this.button_EndProgram.Text = "button2";
            this.button_EndProgram.UseVisualStyleBackColor = true;
            this.button_EndProgram.Click += new System.EventHandler(this.button_EndProgram_Click);
            // 
            // button_Check
            // 
            this.button_Check.Enabled = false;
            this.button_Check.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_Check.Location = new System.Drawing.Point(6, 219);
            this.button_Check.Name = "button_Check";
            this.button_Check.Size = new System.Drawing.Size(77, 33);
            this.button_Check.TabIndex = 1;
            this.button_Check.Text = "button1";
            this.button_Check.UseVisualStyleBackColor = true;
            // 
            // label_QnameText
            // 
            this.label_QnameText.AutoSize = true;
            this.label_QnameText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_QnameText.Location = new System.Drawing.Point(24, 32);
            this.label_QnameText.Name = "label_QnameText";
            this.label_QnameText.Size = new System.Drawing.Size(103, 17);
            this.label_QnameText.TabIndex = 0;
            this.label_QnameText.Text = "label_Qusetion";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 276);
            this.Controls.Add(this.groupBox_Question);
            this.Controls.Add(this.groupBox_Summary);
            this.Controls.Add(this.groupBox_Info);
            this.Controls.Add(this.groupBox_LoadQuiz);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox_LoadQuiz.ResumeLayout(false);
            this.groupBox_Question.ResumeLayout(false);
            this.groupBox_Info.ResumeLayout(false);
            this.groupBox_Info.PerformLayout();
            this.groupBox_Summary.ResumeLayout(false);
            this.groupBox_Summary.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox_LoadQuiz;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label_hello;
        private System.Windows.Forms.Button button_LoadFile;
        private System.Windows.Forms.Label label_Question;
        private System.Windows.Forms.GroupBox groupBox_Question;
        private System.Windows.Forms.Button button_Next;
        private System.Windows.Forms.Button button_Prev;
        private System.Windows.Forms.GroupBox groupBox_Info;
        private System.Windows.Forms.Label label_QuizName;
        private System.Windows.Forms.Label label_QuizOwner;
        private System.Windows.Forms.Label label_QuizVersion;
        private System.Windows.Forms.Label label_QuizDesc;
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.Label label_QuizOwner_Content;
        private System.Windows.Forms.Label label_QuizVersion_Content;
        private System.Windows.Forms.Label label_QuizDesc_Content;
        private System.Windows.Forms.Label label_QuizName_Content;
        private System.Windows.Forms.ListBox listBox_Answers;
        private System.Windows.Forms.GroupBox groupBox_Summary;
        private System.Windows.Forms.Button button_EndProgram;
        private System.Windows.Forms.Button button_Check;
        private System.Windows.Forms.Label label_QnameText;
        private System.Windows.Forms.Label label_QscoreText;
        private System.Windows.Forms.Button button_EndQuiz;
        private System.Windows.Forms.Label label_Qscore;
        private System.Windows.Forms.Label label_Qname;
    }
}

