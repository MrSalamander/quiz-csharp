﻿using System;
using QuizLib;

namespace Quizer
{
    interface IView
    {
        event EventHandler openFile;
        string CurrentLanguage { get; set; }
        Quiz Quiz { get; set; }
        string GetPath { get; }
        void Message(string mes);

    }
}
