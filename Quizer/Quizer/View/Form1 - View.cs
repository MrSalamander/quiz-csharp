﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuizLib;

namespace Quizer
{
    public partial class Form1 : Form, IView
    {
        Properties.Settings s = Properties.Settings.Default;
        ResourceManager resManager = new ResourceManager("Quizer.Language.lang", Assembly.GetExecutingAssembly());
        CultureInfo ci;
        int questionNumber = 0;
        bool lastQuestion = false, firstQuestion = true;
        List<List<bool>> selectedIndex = new List<List<bool>>();
        double score = 0;
        
        public Form1()
        {
            InitializeComponent();
            //openFileDialog1.ShowDialog();
            ci = new CultureInfo(currentLanguage);
            setLanguage();
            showGroupBox_LoadQuiz();
        }

        private Quiz quiz;
        private string path;

        #region Prop

        public event EventHandler openFile;
        public event EventHandler closeProgram;

        private string currentLanguage = "en-US";

        public string CurrentLanguage
        {
            get
            {
                return currentLanguage;
            }
            set
            {
                this.currentLanguage = value;
            }
        }
        public Quiz Quiz
        {
            get
            {
                return quiz;
            }
            set
            {
                quiz = value;
            }
        }
        public string GetPath
        {
            get
            {
                if (this.path.Length > 0) return this.path;
                else return null;
            }
        }

        #endregion 

       
        private void setLanguage()
        {
            //labels
            label_hello.Text = resManager.GetString("hello", ci) + ": ";
            label_QuizName.Text = resManager.GetString("quizName", ci) + ": ";
            label_QuizDesc.Text = resManager.GetString("quizDesc", ci) + ": ";
            label_QuizVersion.Text = resManager.GetString("quizVersion", ci) + ": ";
            label_QuizOwner.Text = resManager.GetString("quizOwner", ci) + ": ";
            label_QnameText.Text = resManager.GetString("quizName", ci) + ": ";
            label_QscoreText.Text = resManager.GetString("score", ci) + ": ";

            //groupBoxes
            groupBox_LoadQuiz.Text = resManager.GetString("loadQuiz", ci);
            groupBox_Info.Text = resManager.GetString("quizInfo", ci);
            groupBox_Question.Text = resManager.GetString("question", ci);
            groupBox_Summary.Text = resManager.GetString("summary", ci);

            //buttons
            button_LoadFile.Text = resManager.GetString("loadQuiz", ci);
            button_Next.Text = resManager.GetString("nextQuestion", ci);
            button_Prev.Text = resManager.GetString("prevQuestion", ci);
            button_Start.Text = resManager.GetString("start", ci);
            button_Check.Text = resManager.GetString("check", ci);
            button_EndProgram.Text = resManager.GetString("endProg", ci);
            button_EndQuiz.Text = resManager.GetString("endQuiz", ci);
            

        }

        public void Message(string mes)
        {
            if(mes.Length > 0) MessageBox.Show(mes);
        }

        private void button_LoadFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            this.path = openFileDialog1.FileName;
            //this.Message(path);
            if (path != "openFileDialog1")
            {
                this.openFile(sender, e);
                showGroupBox_Info();
                label_QuizName_Content.Text = Quiz.Title;
                label_QuizDesc_Content.Text = Quiz.Descryption;
                label_QuizOwner_Content.Text = Quiz.Owner;
                label_QuizVersion_Content.Text = Quiz.Version.ToString();

                //for(int i = 0; i < Quiz.Questions.Count; i++)
                //{
                //    selectedIndex.Add(new List<int>);
                //}
            }
        }
        private void showGroupBox_LoadQuiz()
        {
            groupBox_LoadQuiz.Visible = true;
            groupBox_Question.Visible = false;
            groupBox_Info.Visible = false;
            groupBox_Summary.Visible = false;
        }
        private void showGroupBox_Question()
        {
            groupBox_LoadQuiz.Visible = false;
            groupBox_Question.Visible = true;
            groupBox_Info.Visible = false;
            groupBox_Summary.Visible = false;
        }
        private void showGroupBox_Info()
        {
            groupBox_LoadQuiz.Visible = false;
            groupBox_Question.Visible = false;
            groupBox_Info.Visible = true;
            groupBox_Summary.Visible = false;
        }
        private void showGroupBox_Summary()
        {
            groupBox_LoadQuiz.Visible = false;
            groupBox_Question.Visible = false;
            groupBox_Info.Visible = false;
            groupBox_Summary.Visible = true;
        }

        private void button_Start_Click(object sender, EventArgs e)
        {
            saveIndexes();
            showQuiz();
            //questionNumber++;
        }

        private void showQuiz()
        {
            int iterator = 0;
            showGroupBox_Question();
            //Message(questionNumber.ToString());
            label_Question.Text = Quiz.Questions.ElementAt(questionNumber).Sentance;
            //saveIndexes();

            listBox_Answers.Items.Clear();
            foreach (Answer answer in Quiz.Questions.ElementAt(questionNumber).Answers)
            {
                listBox_Answers.Items.Add(answer.text);
            }
            foreach(bool index in selectedIndex.ElementAt(questionNumber))
            {
                if (index) listBox_Answers.SetSelected(iterator, true);
                iterator++;
            }
            
            //listBox_Answers.sele = selectedIndex.ElementAt(questionNumber);
            checkIfThereIsLastOrFirstQuestion();
        }

        private void checkIfThereIsLastOrFirstQuestion()
        {
            //Message(Quiz.Questions.Count.ToString());
            if ( questionNumber > 0 && questionNumber < Quiz.Questions.Count - 1 )
            {
                button_Prev.Text = resManager.GetString( "prevQuestion", ci );
                button_Next.Text = resManager.GetString( "nextQuestion", ci );
                button_Next.Enabled = true;
                button_Prev.Enabled = true;
                button_EndQuiz.Visible = false;
                button_EndQuiz.Enabled = false;

                //button_Prev.Click += button_Prev_Click;
                //button_Next.Click -= button_Next_Click_2;
                //button_Next.Click += button_Next_Click_1;
            }
            else if (questionNumber == 0)
            {
                button_Next.Text = resManager.GetString("nextQuestion", ci);
                button_Next.Enabled = true;
                //button_Next.Click -= button_Next_Click_2;
                //button_Next.Click += button_Next_Click_1;
                button_Prev.Enabled = false;
                button_EndQuiz.Visible = false;
                button_EndQuiz.Enabled = false;
            }
            else if (questionNumber == Quiz.Questions.Count - 1)
            {
                button_Next.Text = resManager.GetString("end", ci);
                button_Next.Enabled = false;
                button_Prev.Enabled = true;
                button_EndQuiz.Enabled = true;
                button_EndQuiz.Visible = true;
                //button_Next.Click -= button_Next_Click_1;
                //button_Next.Click -= button_Next_Click_1;
                //button_Next.Click += Button_Next_Click;
            }
        }

        void saveIndexes()
        {
            bool isSelected = false;
            if (selectedIndex.Count < questionNumber) selectedIndex.ElementAt(questionNumber).Clear();
            else selectedIndex.Add(new List<bool>());
            selectedIndex.ElementAt(questionNumber).Clear();
            for (int i = 0; i < listBox_Answers.Items.Count; i++)
            {
                string s = (string)listBox_Answers.Items[i];

                //if()listBox_Answers.SetSelected(s, true
                isSelected = listBox_Answers.GetSelected(i);
                selectedIndex.ElementAt(questionNumber).Add(isSelected);
                
                //Message(listBox_Answers.Items[i] + "selected: " + selectedIndex.ElementAt(questionNumber).ElementAt(i));

            }
            //Message("Poszło: " + questionNumber);
            //selectedIndex.RemoveAt(questionNumber);
            //selectedIndex.Insert(questionNumber, listBox_Answers.SelectedIndex);
        }

        private void Button_Next_Click(object sender, EventArgs e)
        {
            //ostatni

            throw new NotImplementedException();
        }

        private void button_Next_Click_1(object sender, EventArgs e)
        {
            // następny
            
            saveIndexes();
            questionNumber++;
            showQuiz();
            
        }

        private void CalculateScore()
        {
            // Od nowa liczenie punktów i koniec

            int iterator1 = 0, iterator2 = 0, wrongs = 0, corrects = 0, currentScore = 0;
            List<double> wages = quiz.Wages;


            foreach( Question question in quiz.Questions )
            {
                iterator2 = 0;
                currentScore = question.Score;
                corrects = wrongs = 0;
                foreach( Answer answer in question.Answers )
                {
                    //Message("Przysło: " + iterator1+" "+iterator2);
                    if (answer.isTrue == selectedIndex.ElementAt(iterator1).ElementAt(iterator2))
                    {
                        corrects++;
                    }
                    else
                    {
                        wrongs++;
                    }

                    iterator2++;
                }
                score += question.Score * countWrongsAndReturnMultiplier(wrongs);
                //Message(score.ToString());
                iterator1++;
            }
            

        }

        private double countWrongsAndReturnMultiplier(int wrongs)
        { 
            for(int iterator = 0; iterator < quiz.Wages.Count; iterator++)
            {
                //Message("błędy: " + wrongs + quiz.Wages[iterator]);
                if (wrongs == iterator)
                {
                    //Message("błędy: " + wrongs + quiz.Wages[iterator]);
                    return quiz.Wages[iterator] / 100.0;
                }
            }
            return 0.0;
        }

        private void endQuiz()
        {
            showGroupBox_Summary();
            CalculateScore();
            label_Qscore.Text = score.ToString();
            label_Qname.Text = Quiz.Title;
        }

        private void buttonEnd_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show(resManager.GetString("endingQuestion", ci), resManager.GetString("endingTitle", ci), MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                saveIndexes();
                endQuiz();
            }
        }

        private void groupBox_Summary_Enter(object sender, EventArgs e)
        {

        }

        private void button_EndProgram_Click(object sender, EventArgs e)
        {
            
        }

        //private void button_Next_Click_1(object sender, MouseEventArgs e)
        //{
        //    questionNumber++;
        //   showQuiz();
        //    
        //    checkIfThereIsLastOrFirstQuestion();
        //}

        private void button_Prev_Click(object sender, EventArgs e)
        {
            //poprzedni
            saveIndexes();
            questionNumber--;
            showQuiz();
        }
    }
}
