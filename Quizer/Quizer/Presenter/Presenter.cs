﻿using Newtonsoft.Json;
using QuizLib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace Quizer
{
    class Presenter
    {
        IView view;
        Model model;
        Properties.Settings s = Properties.Settings.Default;
        ResourceManager resManager = new ResourceManager("Quizer.Language.lang", Assembly.GetExecutingAssembly());
        CultureInfo ci;
        //public Quiz quiz = new Quiz("Test", "Tomasz Kamiński", "0.1", "Testowy quiz zrobiony dla testu. ", new List<double>());
        //public string json = "";
        //public Question q = (new Question("Ile to jest 2 + 2? ", 1, new List<Answer>() { new Answer("1", false), new Answer("2", false), new Answer("3", false), new Answer("4", true) }));
        public Presenter(IView view, Model model)
        {
            this.view = view;
            this.model = model;

            this.view.openFile += openFile;

            //File.Create(@"D:\Projekty\Testowy1");
            //File.WriteAllText(@"D:\Testowy2.json", json);
        }

        private void openFile(object sender, EventArgs e)
        {
            string path = view.GetPath;
            if (path != null)
            {
                try
                {
                    view.Quiz = model.getQuizFromJson(path);
                }
                catch (IOException er)
                {
                    string language = view.CurrentLanguage;
                    view.Message(resManager.GetString("err_file_1", ci) + er.Message);
                    //switch (language)
                    //{
                        //case "en-US":
                            //view.Message(Properties.en_US.err_file_1 + " " + er.Message);
                            //break;
                        //case "pl-PL":
                            //view.Message(Properties.pl_PL.err_file_1 + " " + er.Message);
                            //break;
                        //default:
                            //view.Message(Properties.en_US.err_file_1 + " " + er.Message);
                            //break;
                    //}
                }
                catch(QuizNotCompleted qni)
                {
                    string language = view.CurrentLanguage;
                    view.Message(resManager.GetString("err_file_2", ci) + " " + resManager.GetString("err_file_3", ci) + " " + qni.Message);
                    //switch (language)
                    //{
                        //case "en-US":
                            //view.Message(Properties.en_US.err_file_2 + " " + Properties.en_US.err_file_3 + " " + qni.Message);
                            //break;
                        //case "pl-PL":
                            //view.Message(Properties.pl_PL.err_file_2 + " " + Properties.en_US.err_file_3 + " " + qni.Message);
                            //break;
                        //default:
                            //view.Message(Properties.en_US.err_file_2 + " " + Properties.en_US.err_file_3 + " " + qni.Message);
                            //break;
                    //}
                }
                catch (Exception ex)
                {
                    this.view.Message("Błąd: "+ ex.ToString());
                }
            }
            else view.Message("File is not selected. ");
        }
    }
}
