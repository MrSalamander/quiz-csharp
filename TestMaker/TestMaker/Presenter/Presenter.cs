﻿using System;
using System.Collections.Generic;
using QuizLib;
using TestMaker.Viev;

namespace TestMaker.Presenter
{
	class Presenter
	{
		Model.Model model;
		Viev.Viev viev;

		public Presenter(Model.Model model, Viev.Viev viev)
		{
			this.model = model;
			this.viev = viev;

			viev.LoadQuiz += loadQuiz;

			InitiazePanels();

		}

		private void InitiazePanels()
		{
			//
			// HomePanel
			//
			viev.HomePanel.CreateNewQuiz += viev.createNewQuiz;
			viev.HomePanel.GoToQuizPanel += viev.goToQuizPanel;
			viev.HomePanel.LoadQuiz += viev.loadQuiz;
			//
			// QuizPanel
			//
			viev.QuizPanel.GoToQuestionPanel += viev.goToQuestionPanel;
			viev.QuizPanel.CreateQuiz += createQuiz;
			viev.QuizPanel.GoToHomePanel += viev.goToHomePanel;
			//
			// QuestionPanel
			//
			viev.QuestionPanel.AddQuestion += model.AddQuestion;
			viev.QuestionPanel.GetQuestion += getQuestion;
			viev.QuestionPanel.RemoveQuestion += model.RemoveQuestion;
			viev.QuestionPanel.CountQuestions += countQuestions;
			viev.QuestionPanel.Cancel += ((QuizPanel)viev.QuizPanel).ClearPanel;
			viev.QuestionPanel.Cancel += viev.goToHomePanel;
			viev.QuestionPanel.EditQuestion += model.EditQuestion;
			viev.QuestionPanel.SaveQuiz += safeQuiz;
			viev.QuestionPanel.SaveQuiz += viev.goToHomePanel;
		}




		private Question getQuestion(int i) => model.GetQuestion(i);
		private int countQuestions() => model.CountQuestions();
		//private bool addQuestion(string question, int score, List<QuizLib.Answer> answers) => model.AddQuestion(question, score, answers);
		private void safeQuiz() => model.SaveQuiz();
		private bool createQuiz(string Title, string Author, string Version, string Descryption, uint Duration, List<Double> Wages) => model.CreateQuiz(Title, Author, Version, Descryption, Duration, Wages);
		private Quiz loadQuiz(string Path) => model.LoadQuiz(Path);


	}
}

