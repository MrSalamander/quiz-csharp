﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using QuizLib;

namespace TestMaker.Model
{
	
	class Model
	{
		#region Constants
		const string DEFAULT_SAVES_FOLDER = "saved/ tests";
		#endregion

		#region Fields
		private Quiz quiz;
		private List<Question> questions = new List<Question>();
		#endregion

		#region Methods
		public bool CreateQuiz(string Title, string Author, string Version, string Descryption, uint Duration, List<Double> Wages)
		{
			try
			{
				quiz = new Quiz(Title, Author, Version, Descryption, Duration, Wages);
				questions = new List<Question>();
				return true;
			}
			catch (Exception e)
			{
				Program._log.Info("Model.cs: CreateQuiz() Error: \n" + e.Message);
				return false;
			}
		}

		public bool EditQuiz(string Title, string Author, string Version, string Descryption, uint Duration, List<Double> Wages)
		{
			try
			{
				quiz.Title = Title;
				quiz.Owner = Author;
				quiz.Version = Version;
				quiz.Descryption = Descryption;
				quiz.Duration = Duration;
				quiz.Wages = Wages;
				return true;
			}
			catch (Exception e)
			{
				Program._log.Info("Model.cs: EditQuiz() Error: \n" + e.Message);
				return false;
			}
		}

		public bool AddQuestion(string question, int score , List<Answer> answer)
		{
			try
			{
				questions.Add(new Question(question,score,answer));
				return true;
			}
			catch (Exception e)
			{
				Program._log.Warn("Model.cs: AddQuestion() Error: \n" + e.Message);
				return false;
			}
		}

		public bool EditQuestion(string question, int score, List<Answer> answer, int index)
		{
			try
			{
				questions[index].Sentance = question;
				questions[index].Score = score;
				questions[index].Answers = new List<Answer>(answer);
				return true;
			}
			catch (Exception e)
			{
				Program._log.Info("Model.cs: AddQuestion() Error: \n" + e.Message);
				return false;
			}
		}

		public bool RemoveQuestion(int index)
		{
			try
			{
				questions.RemoveAt(index);
				return true;
			}catch(Exception e)
			{
				Program._log.Warn("Model.cs: RemoveQuestion() Error: \n" + e.Message);
				return false;
			}
		}

		public bool SaveQuiz()
		{
			try
			{
				string PATH_WITH_NAME_4_SAVE = $"{DEFAULT_SAVES_FOLDER}\\{quiz.Title}-{quiz.Version}.json";

				quiz.Questions = questions;
				string json = JsonConvert.SerializeObject(quiz, Formatting.Indented);
				if (!Directory.Exists(DEFAULT_SAVES_FOLDER)) Directory.CreateDirectory(DEFAULT_SAVES_FOLDER);
				File.WriteAllText(PATH_WITH_NAME_4_SAVE, json);
				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
				Program._log.Info("Model.cs: SaveQuiz() Error: \n" + e.Message);
				throw new Exception(e.Message);
			}
		}

		public Quiz LoadQuiz(string Path)
		{
			try
			{
				using (StreamReader file = File.OpenText(Path))
				{
					JsonSerializer serializer = new JsonSerializer();
					quiz = (Quiz)serializer.Deserialize(file, typeof(Quiz));
					questions = quiz.Questions;
					return quiz;
				}
			}catch(Exception e)
			{
				Program._log.Info("Model.cs: LoadQuiz() Error: \n" + e.Message);
				return null;
			}

		}

		public int CountQuestions()
		{
			return questions.Count + 1;
		}

		public Question GetQuestion(int thisQuestion)
		{
			try
			{
				return questions[thisQuestion];
			}catch(Exception)
			{
				return null;
			}
		}
		#endregion
	}
}
