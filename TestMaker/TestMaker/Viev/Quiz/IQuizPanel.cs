﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuizLib;

namespace TestMaker.Viev
{
	interface IQuizPanel
	{
		string Title { get; set; }
		string Version { get; set; }
		string Author { get; set; }
		string Descryption { get; set; }
		List<double> Wages { get; set; }

		Quiz Quiz { get; set; }
		
	}
}
