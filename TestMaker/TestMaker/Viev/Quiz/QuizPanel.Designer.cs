﻿namespace TestMaker.Viev.QuizPanel
{
	partial class QuizPanel
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cancel1Button = new System.Windows.Forms.Button();
			this.wage3Label = new System.Windows.Forms.Label();
			this.wage0Box = new System.Windows.Forms.TextBox();
			this.wage2Label = new System.Windows.Forms.Label();
			this.wage3Box = new System.Windows.Forms.TextBox();
			this.wage1Label = new System.Windows.Forms.Label();
			this.wage2Box = new System.Windows.Forms.TextBox();
			this.wage0Label = new System.Windows.Forms.Label();
			this.wage1Box = new System.Windows.Forms.TextBox();
			this.wageLabel = new System.Windows.Forms.Label();
			this.descriptionBox = new System.Windows.Forms.TextBox();
			this.descriptionLabel = new System.Windows.Forms.Label();
			this.versionBox = new System.Windows.Forms.TextBox();
			this.authorBox = new System.Windows.Forms.TextBox();
			this.titleBox = new System.Windows.Forms.TextBox();
			this.versionLebel = new System.Windows.Forms.Label();
			this.titleLabel = new System.Windows.Forms.Label();
			this.authorLabel = new System.Windows.Forms.Label();
			this.goQuestionsButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// cancel1Button
			// 
			this.cancel1Button.Location = new System.Drawing.Point(37, 401);
			this.cancel1Button.Name = "cancel1Button";
			this.cancel1Button.Size = new System.Drawing.Size(136, 99);
			this.cancel1Button.TabIndex = 32;
			this.cancel1Button.Text = "Anuluj";
			this.cancel1Button.UseVisualStyleBackColor = true;
			// 
			// wage3Label
			// 
			this.wage3Label.AutoSize = true;
			this.wage3Label.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage3Label.Location = new System.Drawing.Point(649, 334);
			this.wage3Label.Name = "wage3Label";
			this.wage3Label.Size = new System.Drawing.Size(55, 36);
			this.wage3Label.TabIndex = 36;
			this.wage3Label.Text = ">=3";
			// 
			// wage0Box
			// 
			this.wage0Box.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage0Box.Location = new System.Drawing.Point(709, 183);
			this.wage0Box.Name = "wage0Box";
			this.wage0Box.Size = new System.Drawing.Size(93, 43);
			this.wage0Box.TabIndex = 25;
			// 
			// wage2Label
			// 
			this.wage2Label.AutoSize = true;
			this.wage2Label.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage2Label.Location = new System.Drawing.Point(660, 285);
			this.wage2Label.Name = "wage2Label";
			this.wage2Label.Size = new System.Drawing.Size(30, 36);
			this.wage2Label.TabIndex = 35;
			this.wage2Label.Text = "2";
			// 
			// wage3Box
			// 
			this.wage3Box.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage3Box.Location = new System.Drawing.Point(709, 331);
			this.wage3Box.Name = "wage3Box";
			this.wage3Box.Size = new System.Drawing.Size(93, 43);
			this.wage3Box.TabIndex = 28;
			// 
			// wage1Label
			// 
			this.wage1Label.AutoSize = true;
			this.wage1Label.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage1Label.Location = new System.Drawing.Point(660, 235);
			this.wage1Label.Name = "wage1Label";
			this.wage1Label.Size = new System.Drawing.Size(30, 36);
			this.wage1Label.TabIndex = 34;
			this.wage1Label.Text = "1";
			// 
			// wage2Box
			// 
			this.wage2Box.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage2Box.Location = new System.Drawing.Point(709, 282);
			this.wage2Box.Name = "wage2Box";
			this.wage2Box.Size = new System.Drawing.Size(93, 43);
			this.wage2Box.TabIndex = 27;
			// 
			// wage0Label
			// 
			this.wage0Label.AutoSize = true;
			this.wage0Label.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage0Label.Location = new System.Drawing.Point(660, 186);
			this.wage0Label.Name = "wage0Label";
			this.wage0Label.Size = new System.Drawing.Size(30, 36);
			this.wage0Label.TabIndex = 33;
			this.wage0Label.Text = "0";
			// 
			// wage1Box
			// 
			this.wage1Box.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage1Box.Location = new System.Drawing.Point(709, 232);
			this.wage1Box.Name = "wage1Box";
			this.wage1Box.Size = new System.Drawing.Size(93, 43);
			this.wage1Box.TabIndex = 26;
			// 
			// wageLabel
			// 
			this.wageLabel.AutoSize = true;
			this.wageLabel.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wageLabel.Location = new System.Drawing.Point(601, 126);
			this.wageLabel.Name = "wageLabel";
			this.wageLabel.Size = new System.Drawing.Size(102, 50);
			this.wageLabel.TabIndex = 31;
			this.wageLabel.Text = "Wagi:";
			// 
			// descriptionBox
			// 
			this.descriptionBox.Location = new System.Drawing.Point(179, 292);
			this.descriptionBox.Multiline = true;
			this.descriptionBox.Name = "descriptionBox";
			this.descriptionBox.Size = new System.Drawing.Size(445, 208);
			this.descriptionBox.TabIndex = 22;
			// 
			// descriptionLabel
			// 
			this.descriptionLabel.AutoSize = true;
			this.descriptionLabel.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.descriptionLabel.Location = new System.Drawing.Point(122, 244);
			this.descriptionLabel.Name = "descriptionLabel";
			this.descriptionLabel.Size = new System.Drawing.Size(84, 50);
			this.descriptionLabel.TabIndex = 29;
			this.descriptionLabel.Text = "Opis";
			// 
			// versionBox
			// 
			this.versionBox.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.versionBox.Location = new System.Drawing.Point(606, 66);
			this.versionBox.Name = "versionBox";
			this.versionBox.Size = new System.Drawing.Size(196, 57);
			this.versionBox.TabIndex = 24;
			// 
			// authorBox
			// 
			this.authorBox.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.authorBox.Location = new System.Drawing.Point(131, 179);
			this.authorBox.Name = "authorBox";
			this.authorBox.Size = new System.Drawing.Size(464, 57);
			this.authorBox.TabIndex = 19;
			// 
			// titleBox
			// 
			this.titleBox.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.titleBox.Location = new System.Drawing.Point(131, 66);
			this.titleBox.Name = "titleBox";
			this.titleBox.Size = new System.Drawing.Size(464, 57);
			this.titleBox.TabIndex = 18;
			// 
			// versionLebel
			// 
			this.versionLebel.AutoSize = true;
			this.versionLebel.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.versionLebel.Location = new System.Drawing.Point(597, 13);
			this.versionLebel.Name = "versionLebel";
			this.versionLebel.Size = new System.Drawing.Size(119, 50);
			this.versionLebel.TabIndex = 23;
			this.versionLebel.Text = "Wersja";
			// 
			// titleLabel
			// 
			this.titleLabel.AutoSize = true;
			this.titleLabel.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.titleLabel.Location = new System.Drawing.Point(122, 13);
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Size = new System.Drawing.Size(101, 50);
			this.titleLabel.TabIndex = 21;
			this.titleLabel.Text = "Tytuł";
			// 
			// authorLabel
			// 
			this.authorLabel.AutoSize = true;
			this.authorLabel.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.authorLabel.Location = new System.Drawing.Point(122, 126);
			this.authorLabel.Name = "authorLabel";
			this.authorLabel.Size = new System.Drawing.Size(108, 50);
			this.authorLabel.TabIndex = 20;
			this.authorLabel.Text = "Autor";
			// 
			// goQuestionsButton
			// 
			this.goQuestionsButton.Location = new System.Drawing.Point(630, 401);
			this.goQuestionsButton.Name = "goQuestionsButton";
			this.goQuestionsButton.Size = new System.Drawing.Size(183, 99);
			this.goQuestionsButton.TabIndex = 30;
			this.goQuestionsButton.Text = "Dalej";
			this.goQuestionsButton.UseVisualStyleBackColor = true;
			// 
			// QuizPanel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.cancel1Button);
			this.Controls.Add(this.wage3Label);
			this.Controls.Add(this.wage0Box);
			this.Controls.Add(this.wage2Label);
			this.Controls.Add(this.wage3Box);
			this.Controls.Add(this.wage1Label);
			this.Controls.Add(this.wage2Box);
			this.Controls.Add(this.wage0Label);
			this.Controls.Add(this.wage1Box);
			this.Controls.Add(this.wageLabel);
			this.Controls.Add(this.descriptionBox);
			this.Controls.Add(this.descriptionLabel);
			this.Controls.Add(this.versionBox);
			this.Controls.Add(this.authorBox);
			this.Controls.Add(this.titleBox);
			this.Controls.Add(this.versionLebel);
			this.Controls.Add(this.titleLabel);
			this.Controls.Add(this.authorLabel);
			this.Controls.Add(this.goQuestionsButton);
			this.Name = "QuizPanel";
			this.Size = new System.Drawing.Size(850, 513);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button cancel1Button;
		private System.Windows.Forms.Label wage3Label;
		private System.Windows.Forms.TextBox wage0Box;
		private System.Windows.Forms.Label wage2Label;
		private System.Windows.Forms.TextBox wage3Box;
		private System.Windows.Forms.Label wage1Label;
		private System.Windows.Forms.TextBox wage2Box;
		private System.Windows.Forms.Label wage0Label;
		private System.Windows.Forms.TextBox wage1Box;
		private System.Windows.Forms.Label wageLabel;
		private System.Windows.Forms.TextBox descriptionBox;
		private System.Windows.Forms.Label descriptionLabel;
		private System.Windows.Forms.TextBox versionBox;
		private System.Windows.Forms.TextBox authorBox;
		private System.Windows.Forms.TextBox titleBox;
		private System.Windows.Forms.Label versionLebel;
		private System.Windows.Forms.Label titleLabel;
		private System.Windows.Forms.Label authorLabel;
		private System.Windows.Forms.Button goQuestionsButton;
	}
}
