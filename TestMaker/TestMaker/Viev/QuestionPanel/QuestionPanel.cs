﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuizLib;

namespace TestMaker.Viev
{

	public partial class QuestionPanel : UserControl, IQuestionPanel
	{
		const int DEFAULT_SCORE_VALUE = 4;
		const string DEFAULT_BOX_TEXT = "";

		#region Fields
		private int currentQuestion;
		private Question question;
		#endregion

		#region Properties
		public List<Answer> Answer
		{
			get
			{
				List<Answer> tmpL = new List<Answer>();
				tmpL.Add(new Answer(answer1Box.Text, isTrue1CheckBox.Checked));
				tmpL.Add(new Answer(answer2Box.Text, isTrue2CheckBox.Checked));
				tmpL.Add(new Answer(answer3Box.Text, isTrue3CheckBox.Checked));
				tmpL.Add(new Answer(answer4Box.Text, isTrue4CheckBox.Checked));
				return tmpL;
			}
			set
			{
				List<Answer> tmp = value;
				if (value != null)
				{
					answer1Box.Text = tmp[0].text;
					answer2Box.Text = tmp[1].text;
					answer3Box.Text = tmp[2].text;
					answer4Box.Text = tmp[3].text;

					isTrue1CheckBox.Checked = tmp[0].isTrue;
					isTrue2CheckBox.Checked = tmp[1].isTrue;
					isTrue3CheckBox.Checked = tmp[2].isTrue;
					isTrue4CheckBox.Checked = tmp[3].isTrue;
				}
				else
				{
					SetDefaultAnswer();
				}

			}
		}

		public string Sentance
		{
			get => questionBox.Text;
			set => questionBox.Text = value;
		}

		public int Score
		{
			get
			{
				try
				{
					if (int.Parse(pointsBox.Text) > 0)
						return int.Parse(pointsBox.Text);
					else throw new ArgumentException("Wynik nie może być niedodatni.");
				}
				catch (Exception)
				{
					MessageBox.Show($"Dobrano punktację domyślną: {DEFAULT_SCORE_VALUE}pt.");
					return DEFAULT_SCORE_VALUE;
				}
			}
			set => pointsBox.Text = value.ToString();
		}

		public Question Question
		{
			get
			{
				return new Question(Sentance, Score, Answer);
			}
			set
			{
				try
				{
					question = value;
					if (value != null)
					{
						Answer = question.Answers;
						Score = question.Score;
						Sentance = question.Sentance;
						UpdateQuestionNumberLabel();
					}
				} catch (Exception)
				{
					ClearPanel();
				}
			}
		}
		#endregion

		#region Events
		public event Func<string, int, List<Answer>, bool> AddQuestion;
		public event Func<string, int, List<Answer>, int, bool> EditQuestion;
		public event Func<int, bool> RemoveQuestion;
		public event Action SaveQuiz;
		public event Action Cancel;
		public event Func<int> CountQuestions;
		public event Func<int, Question> GetQuestion;
		#endregion

		// Constructor
		public QuestionPanel()
		{
			InitializeComponent();
		}



		#region Events
		private void nextQuestionButton_Click(object sender, EventArgs e)
		{

			if (IsNewQuestion())
			{
				if (AddQuestion.Invoke(Sentance, Score, Answer))
				{
					Console.WriteLine("udało się dodać pytanie");
					IncreaseCurrentQuestion();
					ClearPanel();
				}
			}
			else
			{
				EditQuestion?.Invoke(Sentance, Score, Answer, currentQuestion);

				if (IsLastQuestion())
				{
					ClearPanel();
					IncreaseCurrentQuestion();
				}
				else
				{
					GetNextQuestion();
				}
			}
		}

		private void PreviousQuestionButton_Click(object sender, EventArgs e)
		{
			if (currentQuestion > 0)
			{

				GetPreviousQuestion();
			}
		}

		private void SaveQuizButton_Click(object sender, EventArgs e)
		{
			try
			{
				AddQuestion?.Invoke(Sentance, Score, Answer);
				SaveQuiz?.Invoke();
				ClearPanel();
				ResetCounter();
				MessageBox.Show("Poprawnie zapisano test");
			}
			catch (Exception ex)
			{
				MessageBox.Show("Nie udało się zapisać testu \n");
			}
			finally
			{
				ClearPanel();
			}
		}

		private void clearQuestionButton_Click(object sender, EventArgs e)
		{
			ClearPanel();
		}

		private void CancelButton_Click(object sender, EventArgs e)
		{
			ClearPanel();
			ResetCounter();
			Cancel?.Invoke();
		}

		private void RemoveQuestionButton_Click(object sender, EventArgs e)
		{
			RemoveQuestion?.Invoke(currentQuestion);
			Console.WriteLine("currentQuestion " + currentQuestion);
			if (currentQuestion != 0)
			{
			GetPreviousQuestion();
			}
			else
			{
				ClearPanel();
			}
		}
		#endregion


		#region LowAbstractionLevelFunctions
		private void GetNextQuestion()
		{
			Question = GetQuestion?.Invoke(Next_Question());
			currentQuestion++;
			UpdateQuestionNumberLabel();
		}

		private void GetPreviousQuestion()
		{
			Question = GetQuestion?.Invoke(Previous_Question());
			currentQuestion--;
			UpdateQuestionNumberLabel();
		}

		private int Next_Question()
		{
			return currentQuestion + 1;
		}

		private int Previous_Question()
		{
			return currentQuestion - 1;
		}

		public void ResetCounter()
		{
			currentQuestion = 0;
		}

		private void IncreaseCurrentQuestion()
		{
			currentQuestion++;
			UpdateQuestionNumberLabel();
		}

		public void ClearPanel()
		{
			
			Sentance = DEFAULT_BOX_TEXT;
			Answer = null;
			Score = DEFAULT_SCORE_VALUE;
			UpdateQuestionNumberLabel();
		}
	
		private bool IsNewQuestion()
		{
			if (currentQuestion + 1 == CountQuestions?.Invoke())
				return true;
			else
				return false;
		}

		private bool IsLastQuestion()
		{
			if (currentQuestion + 2 == CountQuestions?.Invoke())
				return true;
			else
				return false;
		}

		private void UpdateQuestionNumberLabel()
		{
			questionNumberLabel.Text = string.Format("{0}/{1}", currentQuestion, CountQuestions?.Invoke() - 1  );
		}

		private void SetDefaultAnswer()
		{
			answer1Box.Text = DEFAULT_BOX_TEXT;
			answer2Box.Text = DEFAULT_BOX_TEXT;
			answer3Box.Text = DEFAULT_BOX_TEXT;
			answer4Box.Text = DEFAULT_BOX_TEXT;

			isTrue1CheckBox.Checked = false;
			isTrue2CheckBox.Checked = false;
			isTrue3CheckBox.Checked = false;
			isTrue4CheckBox.Checked = false;
		}
		#endregion

	}
}
