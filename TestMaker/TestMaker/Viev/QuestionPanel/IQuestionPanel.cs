﻿using QuizLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMaker.Viev
{
	public interface IQuestionPanel
	{

		string Sentance { get; set; }
		int Score { get; set; }

		List<Answer> Answer { get; set; }
		Question Question { get; set; }
		//List<Question> Questions { get; set; }

		#region Events
		event Func<string, int, List<Answer>, bool> AddQuestion;
		event Func<string, int, List<Answer>, int, bool> EditQuestion;
		event Func<int, bool> RemoveQuestion;
		event Func<int, Question> GetQuestion;
		event Func<int> CountQuestions;

		
		event Action SaveQuiz;
		event Action Cancel;
		#endregion

		void ClearPanel();
	}
}
