﻿namespace TestMaker.Viev
{
	partial class QuestionPanel
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.previousQuestionButton = new System.Windows.Forms.Button();
			this.questionNumberLabel = new System.Windows.Forms.Label();
			this.cancelButton = new System.Windows.Forms.Button();
			this.isTrueLabel = new System.Windows.Forms.Label();
			this.isTrue4CheckBox = new System.Windows.Forms.CheckBox();
			this.answer4Label = new System.Windows.Forms.Label();
			this.isTrue3CheckBox = new System.Windows.Forms.CheckBox();
			this.answer3Label = new System.Windows.Forms.Label();
			this.isTrue2CheckBox = new System.Windows.Forms.CheckBox();
			this.isTrue1CheckBox = new System.Windows.Forms.CheckBox();
			this.answer2Label = new System.Windows.Forms.Label();
			this.pointsBox = new System.Windows.Forms.TextBox();
			this.answer4Box = new System.Windows.Forms.TextBox();
			this.answer3Box = new System.Windows.Forms.TextBox();
			this.answer2Box = new System.Windows.Forms.TextBox();
			this.answer1Box = new System.Windows.Forms.TextBox();
			this.pointsLabel = new System.Windows.Forms.Label();
			this.answer1Label = new System.Windows.Forms.Label();
			this.questionBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.removeQuestionButton = new System.Windows.Forms.Button();
			this.nextQuestionButton = new System.Windows.Forms.Button();
			this.saveQuizButton = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// previousQuestionButton
			// 
			this.previousQuestionButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.previousQuestionButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(68)))), ((int)(((byte)(99)))));
			this.previousQuestionButton.FlatAppearance.BorderSize = 0;
			this.previousQuestionButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.previousQuestionButton.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.previousQuestionButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.previousQuestionButton.Location = new System.Drawing.Point(512, 487);
			this.previousQuestionButton.Name = "previousQuestionButton";
			this.previousQuestionButton.Size = new System.Drawing.Size(171, 63);
			this.previousQuestionButton.TabIndex = 48;
			this.previousQuestionButton.Text = "Poprzednie";
			this.previousQuestionButton.UseVisualStyleBackColor = false;
			this.previousQuestionButton.Click += new System.EventHandler(this.PreviousQuestionButton_Click);
			// 
			// questionNumberLabel
			// 
			this.questionNumberLabel.AutoSize = true;
			this.questionNumberLabel.Font = new System.Drawing.Font("Segoe Print", 22.09901F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.questionNumberLabel.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.questionNumberLabel.Location = new System.Drawing.Point(16, 38);
			this.questionNumberLabel.Name = "questionNumberLabel";
			this.questionNumberLabel.Size = new System.Drawing.Size(88, 56);
			this.questionNumberLabel.TabIndex = 47;
			this.questionNumberLabel.Text = "0/1";
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.cancelButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(68)))), ((int)(((byte)(99)))));
			this.cancelButton.FlatAppearance.BorderSize = 0;
			this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.cancelButton.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.cancelButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.cancelButton.Location = new System.Drawing.Point(21, 487);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(156, 63);
			this.cancelButton.TabIndex = 46;
			this.cancelButton.Text = "Anuluj";
			this.cancelButton.UseVisualStyleBackColor = false;
			this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
			// 
			// isTrueLabel
			// 
			this.isTrueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.isTrueLabel.AutoSize = true;
			this.isTrueLabel.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.isTrueLabel.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.isTrueLabel.Location = new System.Drawing.Point(703, 170);
			this.isTrueLabel.Name = "isTrueLabel";
			this.isTrueLabel.Size = new System.Drawing.Size(130, 43);
			this.isTrueLabel.TabIndex = 36;
			this.isTrueLabel.Text = "prawda?";
			// 
			// isTrue4CheckBox
			// 
			this.isTrue4CheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.isTrue4CheckBox.AutoSize = true;
			this.isTrue4CheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.isTrue4CheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.isTrue4CheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.isTrue4CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
			this.isTrue4CheckBox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
			this.isTrue4CheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.isTrue4CheckBox.ForeColor = System.Drawing.SystemColors.ActiveCaption;
			this.isTrue4CheckBox.Location = new System.Drawing.Point(757, 433);
			this.isTrue4CheckBox.Name = "isTrue4CheckBox";
			this.isTrue4CheckBox.Size = new System.Drawing.Size(15, 14);
			this.isTrue4CheckBox.TabIndex = 39;
			this.isTrue4CheckBox.UseVisualStyleBackColor = false;
			// 
			// answer4Label
			// 
			this.answer4Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.answer4Label.AutoSize = true;
			this.answer4Label.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.answer4Label.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.answer4Label.Location = new System.Drawing.Point(26, 413);
			this.answer4Label.Name = "answer4Label";
			this.answer4Label.Size = new System.Drawing.Size(106, 43);
			this.answer4Label.TabIndex = 45;
			this.answer4Label.Text = "Odp. 4";
			// 
			// isTrue3CheckBox
			// 
			this.isTrue3CheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.isTrue3CheckBox.AutoSize = true;
			this.isTrue3CheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.isTrue3CheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.isTrue3CheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.isTrue3CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
			this.isTrue3CheckBox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
			this.isTrue3CheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.isTrue3CheckBox.ForeColor = System.Drawing.SystemColors.ActiveCaption;
			this.isTrue3CheckBox.Location = new System.Drawing.Point(757, 369);
			this.isTrue3CheckBox.Name = "isTrue3CheckBox";
			this.isTrue3CheckBox.Size = new System.Drawing.Size(15, 14);
			this.isTrue3CheckBox.TabIndex = 38;
			this.isTrue3CheckBox.UseVisualStyleBackColor = false;
			// 
			// answer3Label
			// 
			this.answer3Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.answer3Label.AutoSize = true;
			this.answer3Label.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.answer3Label.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.answer3Label.Location = new System.Drawing.Point(26, 349);
			this.answer3Label.Name = "answer3Label";
			this.answer3Label.Size = new System.Drawing.Size(106, 43);
			this.answer3Label.TabIndex = 44;
			this.answer3Label.Text = "Odp. 3";
			// 
			// isTrue2CheckBox
			// 
			this.isTrue2CheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.isTrue2CheckBox.AutoSize = true;
			this.isTrue2CheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.isTrue2CheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.isTrue2CheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.isTrue2CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
			this.isTrue2CheckBox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
			this.isTrue2CheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.isTrue2CheckBox.ForeColor = System.Drawing.SystemColors.ActiveCaption;
			this.isTrue2CheckBox.Location = new System.Drawing.Point(757, 302);
			this.isTrue2CheckBox.Name = "isTrue2CheckBox";
			this.isTrue2CheckBox.Size = new System.Drawing.Size(15, 14);
			this.isTrue2CheckBox.TabIndex = 37;
			this.isTrue2CheckBox.UseVisualStyleBackColor = false;
			// 
			// isTrue1CheckBox
			// 
			this.isTrue1CheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.isTrue1CheckBox.AutoSize = true;
			this.isTrue1CheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.isTrue1CheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.isTrue1CheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.isTrue1CheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
			this.isTrue1CheckBox.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
			this.isTrue1CheckBox.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.isTrue1CheckBox.ForeColor = System.Drawing.SystemColors.ActiveCaption;
			this.isTrue1CheckBox.Location = new System.Drawing.Point(757, 238);
			this.isTrue1CheckBox.Name = "isTrue1CheckBox";
			this.isTrue1CheckBox.Size = new System.Drawing.Size(15, 14);
			this.isTrue1CheckBox.TabIndex = 34;
			this.isTrue1CheckBox.UseVisualStyleBackColor = false;
			// 
			// answer2Label
			// 
			this.answer2Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.answer2Label.AutoSize = true;
			this.answer2Label.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.answer2Label.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.answer2Label.Location = new System.Drawing.Point(26, 282);
			this.answer2Label.Name = "answer2Label";
			this.answer2Label.Size = new System.Drawing.Size(106, 43);
			this.answer2Label.TabIndex = 43;
			this.answer2Label.Text = "Odp. 2";
			// 
			// pointsBox
			// 
			this.pointsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pointsBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.pointsBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.pointsBox.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.pointsBox.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.pointsBox.Location = new System.Drawing.Point(732, 101);
			this.pointsBox.Name = "pointsBox";
			this.pointsBox.Size = new System.Drawing.Size(90, 45);
			this.pointsBox.TabIndex = 32;
			// 
			// answer4Box
			// 
			this.answer4Box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.answer4Box.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.answer4Box.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.answer4Box.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.answer4Box.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.answer4Box.Location = new System.Drawing.Point(146, 413);
			this.answer4Box.Name = "answer4Box";
			this.answer4Box.Size = new System.Drawing.Size(550, 45);
			this.answer4Box.TabIndex = 31;
			// 
			// answer3Box
			// 
			this.answer3Box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.answer3Box.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.answer3Box.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.answer3Box.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.answer3Box.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.answer3Box.Location = new System.Drawing.Point(146, 347);
			this.answer3Box.Name = "answer3Box";
			this.answer3Box.Size = new System.Drawing.Size(550, 45);
			this.answer3Box.TabIndex = 30;
			// 
			// answer2Box
			// 
			this.answer2Box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.answer2Box.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.answer2Box.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.answer2Box.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.answer2Box.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.answer2Box.Location = new System.Drawing.Point(146, 282);
			this.answer2Box.Name = "answer2Box";
			this.answer2Box.Size = new System.Drawing.Size(550, 45);
			this.answer2Box.TabIndex = 28;
			// 
			// answer1Box
			// 
			this.answer1Box.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.answer1Box.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.answer1Box.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.answer1Box.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.answer1Box.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.answer1Box.Location = new System.Drawing.Point(146, 218);
			this.answer1Box.Name = "answer1Box";
			this.answer1Box.Size = new System.Drawing.Size(550, 45);
			this.answer1Box.TabIndex = 27;
			// 
			// pointsLabel
			// 
			this.pointsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pointsLabel.AutoSize = true;
			this.pointsLabel.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.pointsLabel.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.pointsLabel.Location = new System.Drawing.Point(703, 38);
			this.pointsLabel.Name = "pointsLabel";
			this.pointsLabel.Size = new System.Drawing.Size(149, 43);
			this.pointsLabel.TabIndex = 35;
			this.pointsLabel.Text = "Za ile pkt.";
			// 
			// answer1Label
			// 
			this.answer1Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.answer1Label.AutoSize = true;
			this.answer1Label.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.answer1Label.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.answer1Label.Location = new System.Drawing.Point(26, 218);
			this.answer1Label.Name = "answer1Label";
			this.answer1Label.Size = new System.Drawing.Size(106, 43);
			this.answer1Label.TabIndex = 33;
			this.answer1Label.Text = "Odp. 1";
			// 
			// questionBox
			// 
			this.questionBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.questionBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.questionBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.questionBox.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.questionBox.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.questionBox.Location = new System.Drawing.Point(146, 64);
			this.questionBox.Multiline = true;
			this.questionBox.Name = "questionBox";
			this.questionBox.Size = new System.Drawing.Size(551, 134);
			this.questionBox.TabIndex = 26;
			// 
			// label3
			// 
			this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label3.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.label3.Location = new System.Drawing.Point(21, 151);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(116, 43);
			this.label3.TabIndex = 29;
			this.label3.Text = "Pytanie";
			// 
			// removeQuestionButton
			// 
			this.removeQuestionButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.removeQuestionButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(68)))), ((int)(((byte)(99)))));
			this.removeQuestionButton.FlatAppearance.BorderSize = 0;
			this.removeQuestionButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.removeQuestionButton.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.removeQuestionButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.removeQuestionButton.Location = new System.Drawing.Point(355, 487);
			this.removeQuestionButton.Name = "removeQuestionButton";
			this.removeQuestionButton.Size = new System.Drawing.Size(151, 63);
			this.removeQuestionButton.TabIndex = 41;
			this.removeQuestionButton.Text = "Usuń";
			this.removeQuestionButton.UseVisualStyleBackColor = false;
			this.removeQuestionButton.Click += new System.EventHandler(this.RemoveQuestionButton_Click);
			// 
			// nextQuestionButton
			// 
			this.nextQuestionButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.nextQuestionButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(68)))), ((int)(((byte)(99)))));
			this.nextQuestionButton.FlatAppearance.BorderSize = 0;
			this.nextQuestionButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.nextQuestionButton.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.nextQuestionButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.nextQuestionButton.Location = new System.Drawing.Point(689, 487);
			this.nextQuestionButton.Name = "nextQuestionButton";
			this.nextQuestionButton.Size = new System.Drawing.Size(165, 63);
			this.nextQuestionButton.TabIndex = 40;
			this.nextQuestionButton.Text = "Następne";
			this.nextQuestionButton.UseVisualStyleBackColor = false;
			this.nextQuestionButton.Click += new System.EventHandler(this.nextQuestionButton_Click);
			// 
			// saveQuizButton
			// 
			this.saveQuizButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.saveQuizButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(68)))), ((int)(((byte)(99)))));
			this.saveQuizButton.FlatAppearance.BorderSize = 0;
			this.saveQuizButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.saveQuizButton.Font = new System.Drawing.Font("Segoe Print", 17.82178F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.saveQuizButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.saveQuizButton.Location = new System.Drawing.Point(182, 487);
			this.saveQuizButton.Name = "saveQuizButton";
			this.saveQuizButton.Size = new System.Drawing.Size(167, 63);
			this.saveQuizButton.TabIndex = 42;
			this.saveQuizButton.Text = "Zakończ";
			this.saveQuizButton.UseVisualStyleBackColor = false;
			this.saveQuizButton.Click += new System.EventHandler(this.SaveQuizButton_Click);
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(80)))), ((int)(((byte)(77)))));
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(867, 35);
			this.panel1.TabIndex = 49;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(80)))), ((int)(((byte)(77)))));
			this.panel2.Controls.Add(this.questionNumberLabel);
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(132, 98);
			this.panel2.TabIndex = 50;
			// 
			// QuestionPanel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.previousQuestionButton);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.isTrueLabel);
			this.Controls.Add(this.isTrue4CheckBox);
			this.Controls.Add(this.answer4Label);
			this.Controls.Add(this.isTrue3CheckBox);
			this.Controls.Add(this.answer3Label);
			this.Controls.Add(this.isTrue2CheckBox);
			this.Controls.Add(this.isTrue1CheckBox);
			this.Controls.Add(this.answer2Label);
			this.Controls.Add(this.pointsBox);
			this.Controls.Add(this.answer4Box);
			this.Controls.Add(this.answer3Box);
			this.Controls.Add(this.answer2Box);
			this.Controls.Add(this.answer1Box);
			this.Controls.Add(this.pointsLabel);
			this.Controls.Add(this.answer1Label);
			this.Controls.Add(this.questionBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.removeQuestionButton);
			this.Controls.Add(this.nextQuestionButton);
			this.Controls.Add(this.saveQuizButton);
			this.Name = "QuestionPanel";
			this.Size = new System.Drawing.Size(867, 571);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button previousQuestionButton;
		private System.Windows.Forms.Label questionNumberLabel;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Label isTrueLabel;
		private System.Windows.Forms.CheckBox isTrue4CheckBox;
		private System.Windows.Forms.Label answer4Label;
		private System.Windows.Forms.CheckBox isTrue3CheckBox;
		private System.Windows.Forms.Label answer3Label;
		private System.Windows.Forms.CheckBox isTrue2CheckBox;
		private System.Windows.Forms.CheckBox isTrue1CheckBox;
		private System.Windows.Forms.Label answer2Label;
		private System.Windows.Forms.TextBox pointsBox;
		private System.Windows.Forms.TextBox answer4Box;
		private System.Windows.Forms.TextBox answer3Box;
		private System.Windows.Forms.TextBox answer2Box;
		private System.Windows.Forms.TextBox answer1Box;
		private System.Windows.Forms.Label pointsLabel;
		private System.Windows.Forms.Label answer1Label;
		private System.Windows.Forms.TextBox questionBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button removeQuestionButton;
		private System.Windows.Forms.Button nextQuestionButton;
		private System.Windows.Forms.Button saveQuizButton;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
	}
}
