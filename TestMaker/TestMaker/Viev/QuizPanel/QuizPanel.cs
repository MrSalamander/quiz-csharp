﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using QuizLib;

namespace TestMaker.Viev
{


	public partial class QuizPanel : UserControl, IQuizPanel
	{
		const uint DEFAULT_DURATION_VALUE = 15;
		const string DEFAULT_BOX_TEXT = "";

		private Quiz quiz;

		#region Properties
		public Quiz Quiz
		{
			get
			{
				quiz = new Quiz(Title, Author, Version, Descryption, Duration, Wages);
				return quiz;
			}
			set
			{
				quiz = value;
				FillBoxesWithQuizContent();
			}
		}
		public string Title {
			get => titleBox.Text;
			set => titleBox.Text = value;
		}
		public string Version {
			get => versionBox.Text;
			set => versionBox.Text = value;
		}
		public string Author {
			get => authorBox.Text;
			set => authorBox.Text = value;
		}
		public string Descryption {
			get => descriptionBox.Text; 
			set => descriptionBox.Text = value;
		}
		public List<double> Wages
		{
			get
			{
				List<double> tmp = new List<double>();
				try
				{
					tmp.Add(Double.Parse(wage0Box.Text));
					tmp.Add(Double.Parse(wage1Box.Text));
					tmp.Add(Double.Parse(wage2Box.Text));
					tmp.Add(Double.Parse(wage3Box.Text));
				}
				catch (Exception e)
				{
					Program._log.Info("QuizPanel.cs/Wages: Getter error. Default values returned. \n" + e.Message);
					tmp = ReturnDefaultWages();
				}
				return tmp;
				
			}
			set
			{
				List<double> tmp = value;
				try { 
					wage0Box.Text = tmp[0].ToString();
					wage1Box.Text = tmp[1].ToString();
					wage2Box.Text = tmp[2].ToString();
					wage3Box.Text = tmp[3].ToString();
				}
				catch(Exception e)
				{
					Program._log.Info("QuizPanel.cs/Wages: Setter error. Default values loaded. \n" + e.Message);
					SetDefaultWages();
				}
			}

		}

		public uint Duration
		{
			get
			{
				try
				{
					uint minutes = ValidateDuration(uint.Parse(durationBox.Text));
					return (60 * 1000 * minutes); // in miliseconds
				}catch(Exception e)
				{
					Program._log.Warn("QuizPanel.cs/Duration: Getter error. Default values loaded. \n" + e.Message);
					return (60 * 1000 * DEFAULT_DURATION_VALUE);
				}
			
			}
			set => durationBox.Text = (value/60/1000).ToString(); // in minutes
		}

		public bool Editing { get; set; }
		#endregion

		#region Events
		public event Action GoToQuestionPanel;
		public event Action GoToHomePanel;
		public event Func<string, string, string, string, uint, List<double>, bool> CreateQuiz;
		public event Func<string, string, string, string, uint, List<double>, bool> EditQuiz;
		#endregion



		private void goQuestionsButton_Click(object sender, EventArgs e)
		{
			if(Editing)
			{
				EditQuiz?.Invoke(Title, Author, Version, Descryption, Duration, Wages);
			}
			else
			{
				CreateQuiz?.Invoke(Title, Author, Version, Descryption, Duration, Wages);
			}
			GoToQuestionPanel?.Invoke();
		}

		private void cancel1Button_Click(object sender, EventArgs e)
		{

			GoToHomePanel?.Invoke();
			this.ClearPanel();
		}




		#region Low_Abstraction_Methods
		private void FillBoxesWithQuizContent()
		{
			try
			{
				Title = quiz.Title;
				Version = quiz.Version;
				Author = quiz.Owner;
				Descryption = quiz.Descryption;
				Duration = quiz.Duration;
				Wages = quiz.Wages;
			}
			catch (Exception e)
			{
				Program._log.Info("QuizPanel.cs/fillBoxesWithQuizContent(): Setter error. Default values loaded. \n" + e.Message);
				ClearAllTextBoxes();
				SetDefaultWages();
			}
		}

		public void ClearPanel()
		{
			Quiz = null; // will execute clearAllTextBoxes() and setDefaultWages()
		}

		private uint ValidateDuration(uint minutes)
		{
			if (minutes > 0)
			{
				return minutes;
			}
			else
			{
				Program._log.Warn("QuizPanel.cs/Duration: Getter error: Wrong value. Default value loaded. \n");
				throw new ArgumentException("Duration should be more than 0.");
			}
		}

		private void ClearAllTextBoxes()
		{
			Title = DEFAULT_BOX_TEXT;
			Version = DEFAULT_BOX_TEXT;
			Author = DEFAULT_BOX_TEXT;
			Descryption = DEFAULT_BOX_TEXT;
			Duration = DEFAULT_DURATION_VALUE;
		}

		private void SetDefaultWages()
		{
			wage0Box.Text = "100";
			wage1Box.Text = "50";
			wage2Box.Text = "0";
			wage3Box.Text = "0";
		}

		private List<double> ReturnDefaultWages()
		{
			List<double> tmp = new List<double>();
			tmp.Add(100);
			tmp.Add(50);
			tmp.Add(0);
			tmp.Add(0);
			return tmp;
		}
		#endregion


		// Constructor
		public QuizPanel()
		{
			InitializeComponent();
		}

	}
}
