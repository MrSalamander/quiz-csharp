﻿using QuizLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMaker.Viev
{
	public interface IQuizPanel
	{
		#region Properties
		Quiz Quiz { get; set; }
		string Title { get; set; }
		string Version { get; set; }
		string Author { get; set; }
		string Descryption { get; set; }
		uint Duration { get; set; }
		List<double> Wages { get; set; }

		bool Editing { get; set; }
		#endregion

		#region Events
		event Action GoToQuestionPanel;
		event Action GoToHomePanel;
		event Func<string, string, string, string, uint, List<Double>, bool> CreateQuiz;

		#endregion
	}
}
