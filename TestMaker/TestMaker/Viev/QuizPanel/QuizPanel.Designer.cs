﻿namespace TestMaker.Viev
{
	partial class QuizPanel
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuizPanel));
			this.cancel1Button = new System.Windows.Forms.Button();
			this.wage3Label = new System.Windows.Forms.Label();
			this.wage0Box = new System.Windows.Forms.TextBox();
			this.wage2Label = new System.Windows.Forms.Label();
			this.wage3Box = new System.Windows.Forms.TextBox();
			this.wage1Label = new System.Windows.Forms.Label();
			this.wage2Box = new System.Windows.Forms.TextBox();
			this.wage0Label = new System.Windows.Forms.Label();
			this.wage1Box = new System.Windows.Forms.TextBox();
			this.wageLabel = new System.Windows.Forms.Label();
			this.descriptionBox = new System.Windows.Forms.TextBox();
			this.descriptionLabel = new System.Windows.Forms.Label();
			this.versionBox = new System.Windows.Forms.TextBox();
			this.authorBox = new System.Windows.Forms.TextBox();
			this.titleBox = new System.Windows.Forms.TextBox();
			this.versionLebel = new System.Windows.Forms.Label();
			this.titleLabel = new System.Windows.Forms.Label();
			this.authorLabel = new System.Windows.Forms.Label();
			this.goQuestionsButton = new System.Windows.Forms.Button();
			this.durationBox = new System.Windows.Forms.TextBox();
			this.quizDurationLabel = new System.Windows.Forms.Label();
			this.quizDurationToolTip = new System.Windows.Forms.ToolTip(this.components);
			this.wagesToolTip = new System.Windows.Forms.ToolTip(this.components);
			this.SuspendLayout();
			// 
			// cancel1Button
			// 
			this.cancel1Button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(68)))), ((int)(((byte)(99)))));
			this.cancel1Button.FlatAppearance.BorderSize = 0;
			this.cancel1Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.cancel1Button.Font = new System.Drawing.Font("Segoe Print", 19.24752F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.cancel1Button.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.cancel1Button.Location = new System.Drawing.Point(525, 424);
			this.cancel1Button.Name = "cancel1Button";
			this.cancel1Button.Size = new System.Drawing.Size(141, 99);
			this.cancel1Button.TabIndex = 10;
			this.cancel1Button.Text = "Anuluj";
			this.cancel1Button.UseVisualStyleBackColor = false;
			this.cancel1Button.Click += new System.EventHandler(this.cancel1Button_Click);
			// 
			// wage3Label
			// 
			this.wage3Label.AutoSize = true;
			this.wage3Label.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage3Label.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.wage3Label.Location = new System.Drawing.Point(574, 339);
			this.wage3Label.Name = "wage3Label";
			this.wage3Label.Size = new System.Drawing.Size(54, 35);
			this.wage3Label.TabIndex = 36;
			this.wage3Label.Text = ">=3";
			this.wagesToolTip.SetToolTip(this.wage3Label, resources.GetString("wage3Label.ToolTip"));
			// 
			// wage0Box
			// 
			this.wage0Box.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.wage0Box.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.wage0Box.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage0Box.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.wage0Box.Location = new System.Drawing.Point(634, 188);
			this.wage0Box.Name = "wage0Box";
			this.wage0Box.Size = new System.Drawing.Size(115, 34);
			this.wage0Box.TabIndex = 5;
			this.quizDurationToolTip.SetToolTip(this.wage0Box, resources.GetString("wage0Box.ToolTip"));
			// 
			// wage2Label
			// 
			this.wage2Label.AutoSize = true;
			this.wage2Label.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage2Label.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.wage2Label.Location = new System.Drawing.Point(585, 290);
			this.wage2Label.Name = "wage2Label";
			this.wage2Label.Size = new System.Drawing.Size(30, 35);
			this.wage2Label.TabIndex = 35;
			this.wage2Label.Text = "2";
			this.wagesToolTip.SetToolTip(this.wage2Label, resources.GetString("wage2Label.ToolTip"));
			// 
			// wage3Box
			// 
			this.wage3Box.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.wage3Box.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.wage3Box.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage3Box.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.wage3Box.Location = new System.Drawing.Point(634, 336);
			this.wage3Box.Name = "wage3Box";
			this.wage3Box.Size = new System.Drawing.Size(115, 34);
			this.wage3Box.TabIndex = 8;
			this.quizDurationToolTip.SetToolTip(this.wage3Box, resources.GetString("wage3Box.ToolTip"));
			// 
			// wage1Label
			// 
			this.wage1Label.AutoSize = true;
			this.wage1Label.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage1Label.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.wage1Label.Location = new System.Drawing.Point(585, 240);
			this.wage1Label.Name = "wage1Label";
			this.wage1Label.Size = new System.Drawing.Size(30, 35);
			this.wage1Label.TabIndex = 34;
			this.wage1Label.Text = "1";
			this.wagesToolTip.SetToolTip(this.wage1Label, resources.GetString("wage1Label.ToolTip"));
			// 
			// wage2Box
			// 
			this.wage2Box.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.wage2Box.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.wage2Box.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage2Box.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.wage2Box.Location = new System.Drawing.Point(634, 287);
			this.wage2Box.Name = "wage2Box";
			this.wage2Box.Size = new System.Drawing.Size(115, 34);
			this.wage2Box.TabIndex = 7;
			this.quizDurationToolTip.SetToolTip(this.wage2Box, resources.GetString("wage2Box.ToolTip"));
			// 
			// wage0Label
			// 
			this.wage0Label.AutoSize = true;
			this.wage0Label.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage0Label.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.wage0Label.Location = new System.Drawing.Point(585, 191);
			this.wage0Label.Name = "wage0Label";
			this.wage0Label.Size = new System.Drawing.Size(30, 35);
			this.wage0Label.TabIndex = 33;
			this.wage0Label.Text = "0";
			this.wagesToolTip.SetToolTip(this.wage0Label, resources.GetString("wage0Label.ToolTip"));
			// 
			// wage1Box
			// 
			this.wage1Box.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.wage1Box.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.wage1Box.Font = new System.Drawing.Font("Segoe Print", 14.25743F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wage1Box.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.wage1Box.Location = new System.Drawing.Point(634, 237);
			this.wage1Box.Name = "wage1Box";
			this.wage1Box.Size = new System.Drawing.Size(115, 34);
			this.wage1Box.TabIndex = 6;
			this.quizDurationToolTip.SetToolTip(this.wage1Box, resources.GetString("wage1Box.ToolTip"));
			// 
			// wageLabel
			// 
			this.wageLabel.AutoSize = true;
			this.wageLabel.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.wageLabel.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.wageLabel.Location = new System.Drawing.Point(582, 131);
			this.wageLabel.Name = "wageLabel";
			this.wageLabel.Size = new System.Drawing.Size(95, 47);
			this.wageLabel.TabIndex = 31;
			this.wageLabel.Text = "Wagi:";
			this.wagesToolTip.SetToolTip(this.wageLabel, resources.GetString("wageLabel.ToolTip"));
			// 
			// descriptionBox
			// 
			this.descriptionBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.descriptionBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.descriptionBox.Font = new System.Drawing.Font("Segoe Print", 14.24752F);
			this.descriptionBox.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.descriptionBox.Location = new System.Drawing.Point(24, 292);
			this.descriptionBox.Multiline = true;
			this.descriptionBox.Name = "descriptionBox";
			this.descriptionBox.Size = new System.Drawing.Size(495, 231);
			this.descriptionBox.TabIndex = 2;
			// 
			// descriptionLabel
			// 
			this.descriptionLabel.AutoSize = true;
			this.descriptionLabel.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.descriptionLabel.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.descriptionLabel.Location = new System.Drawing.Point(15, 237);
			this.descriptionLabel.Name = "descriptionLabel";
			this.descriptionLabel.Size = new System.Drawing.Size(78, 47);
			this.descriptionLabel.TabIndex = 29;
			this.descriptionLabel.Text = "Opis";
			// 
			// versionBox
			// 
			this.versionBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.versionBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.versionBox.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.versionBox.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.versionBox.Location = new System.Drawing.Point(567, 61);
			this.versionBox.Name = "versionBox";
			this.versionBox.Size = new System.Drawing.Size(128, 47);
			this.versionBox.TabIndex = 3;
			// 
			// authorBox
			// 
			this.authorBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.authorBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.authorBox.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.authorBox.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.authorBox.Location = new System.Drawing.Point(24, 174);
			this.authorBox.Name = "authorBox";
			this.authorBox.Size = new System.Drawing.Size(526, 47);
			this.authorBox.TabIndex = 1;
			// 
			// titleBox
			// 
			this.titleBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.titleBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.titleBox.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.titleBox.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.titleBox.Location = new System.Drawing.Point(24, 61);
			this.titleBox.Name = "titleBox";
			this.titleBox.Size = new System.Drawing.Size(526, 47);
			this.titleBox.TabIndex = 0;
			// 
			// versionLebel
			// 
			this.versionLebel.AutoSize = true;
			this.versionLebel.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.versionLebel.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.versionLebel.Location = new System.Drawing.Point(558, 8);
			this.versionLebel.Name = "versionLebel";
			this.versionLebel.Size = new System.Drawing.Size(111, 47);
			this.versionLebel.TabIndex = 23;
			this.versionLebel.Text = "Wersja";
			// 
			// titleLabel
			// 
			this.titleLabel.AutoSize = true;
			this.titleLabel.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.titleLabel.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.titleLabel.Location = new System.Drawing.Point(15, 8);
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Size = new System.Drawing.Size(93, 47);
			this.titleLabel.TabIndex = 21;
			this.titleLabel.Text = "Tytuł";
			// 
			// authorLabel
			// 
			this.authorLabel.AutoSize = true;
			this.authorLabel.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.authorLabel.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.authorLabel.Location = new System.Drawing.Point(15, 121);
			this.authorLabel.Name = "authorLabel";
			this.authorLabel.Size = new System.Drawing.Size(99, 47);
			this.authorLabel.TabIndex = 20;
			this.authorLabel.Text = "Autor";
			// 
			// goQuestionsButton
			// 
			this.goQuestionsButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(68)))), ((int)(((byte)(99)))));
			this.goQuestionsButton.FlatAppearance.BorderSize = 0;
			this.goQuestionsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.goQuestionsButton.Font = new System.Drawing.Font("Segoe Print", 19.24752F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.goQuestionsButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.goQuestionsButton.Location = new System.Drawing.Point(672, 424);
			this.goQuestionsButton.Name = "goQuestionsButton";
			this.goQuestionsButton.Size = new System.Drawing.Size(141, 99);
			this.goQuestionsButton.TabIndex = 9;
			this.goQuestionsButton.Text = "Dalej";
			this.goQuestionsButton.UseVisualStyleBackColor = false;
			this.goQuestionsButton.Click += new System.EventHandler(this.goQuestionsButton_Click);
			// 
			// durationBox
			// 
			this.durationBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
			this.durationBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.durationBox.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.durationBox.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.durationBox.Location = new System.Drawing.Point(712, 61);
			this.durationBox.Name = "durationBox";
			this.durationBox.Size = new System.Drawing.Size(128, 47);
			this.durationBox.TabIndex = 4;
			this.quizDurationToolTip.SetToolTip(this.durationBox, "Czas przeznaczony na rozwiązanie tego testu,\r\nWyrażany w minutach [1-60].\r\n\r\n");
			// 
			// quizDurationLabel
			// 
			this.quizDurationLabel.AutoSize = true;
			this.quizDurationLabel.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.quizDurationLabel.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.quizDurationLabel.Location = new System.Drawing.Point(703, 8);
			this.quizDurationLabel.Name = "quizDurationLabel";
			this.quizDurationLabel.Size = new System.Drawing.Size(88, 47);
			this.quizDurationLabel.TabIndex = 37;
			this.quizDurationLabel.Text = "Czas:";
			this.quizDurationToolTip.SetToolTip(this.quizDurationLabel, "Czas przeznaczony na rozwiązanie tego testu,\r\nWyrażany w minutach [1-60].\r\n\r\n");
			// 
			// QuizPanel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
			this.Controls.Add(this.durationBox);
			this.Controls.Add(this.quizDurationLabel);
			this.Controls.Add(this.cancel1Button);
			this.Controls.Add(this.wage3Label);
			this.Controls.Add(this.wage0Box);
			this.Controls.Add(this.wage2Label);
			this.Controls.Add(this.wage3Box);
			this.Controls.Add(this.wage1Label);
			this.Controls.Add(this.wage2Box);
			this.Controls.Add(this.wage0Label);
			this.Controls.Add(this.wage1Box);
			this.Controls.Add(this.wageLabel);
			this.Controls.Add(this.descriptionBox);
			this.Controls.Add(this.descriptionLabel);
			this.Controls.Add(this.versionBox);
			this.Controls.Add(this.authorBox);
			this.Controls.Add(this.titleBox);
			this.Controls.Add(this.versionLebel);
			this.Controls.Add(this.titleLabel);
			this.Controls.Add(this.authorLabel);
			this.Controls.Add(this.goQuestionsButton);
			this.Name = "QuizPanel";
			this.Size = new System.Drawing.Size(853, 553);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button cancel1Button;
		private System.Windows.Forms.Label wage3Label;
		private System.Windows.Forms.TextBox wage0Box;
		private System.Windows.Forms.Label wage2Label;
		private System.Windows.Forms.TextBox wage3Box;
		private System.Windows.Forms.Label wage1Label;
		private System.Windows.Forms.TextBox wage2Box;
		private System.Windows.Forms.Label wage0Label;
		private System.Windows.Forms.TextBox wage1Box;
		private System.Windows.Forms.Label wageLabel;
		private System.Windows.Forms.TextBox descriptionBox;
		private System.Windows.Forms.Label descriptionLabel;
		private System.Windows.Forms.TextBox versionBox;
		private System.Windows.Forms.TextBox authorBox;
		private System.Windows.Forms.TextBox titleBox;
		private System.Windows.Forms.Label versionLebel;
		private System.Windows.Forms.Label titleLabel;
		private System.Windows.Forms.Label authorLabel;
		private System.Windows.Forms.Button goQuestionsButton;
		private System.Windows.Forms.TextBox durationBox;
		private System.Windows.Forms.Label quizDurationLabel;
		private System.Windows.Forms.ToolTip quizDurationToolTip;
		private System.Windows.Forms.ToolTip wagesToolTip;
	}
}
