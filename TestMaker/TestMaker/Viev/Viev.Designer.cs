﻿namespace TestMaker.Viev
{
	partial class Viev
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Viev));
			this.mainPanel = new TestMaker.Viev.HomePanel();
			this.quizPanel = new TestMaker.Viev.QuizPanel();
			this.questionPanel = new TestMaker.Viev.QuestionPanel();
			this.SuspendLayout();
			// 
			// mainPanel
			// 
			this.mainPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
			this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainPanel.Location = new System.Drawing.Point(0, 0);
			this.mainPanel.Name = "mainPanel";
			this.mainPanel.Size = new System.Drawing.Size(873, 554);
			this.mainPanel.TabIndex = 0;
			// 
			// quizPanel
			// 
			this.quizPanel.Author = "";
			this.quizPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
			this.quizPanel.Descryption = "";
			this.quizPanel.Location = new System.Drawing.Point(1, 0);
			this.quizPanel.Name = "quizPanel";
			this.quizPanel.Quiz = ((QuizLib.Quiz)(resources.GetObject("quizPanel.Quiz")));
			this.quizPanel.Size = new System.Drawing.Size(866, 553);
			this.quizPanel.TabIndex = 1;
			this.quizPanel.Title = "";
			this.quizPanel.Version = "";
			this.quizPanel.Wages = ((System.Collections.Generic.List<double>)(resources.GetObject("quizPanel.Wages")));
			// 
			// questionPanel
			// 
			this.questionPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.questionPanel.Answer = ((System.Collections.Generic.List<QuizLib.Answer>)(resources.GetObject("questionPanel.Answer")));
			this.questionPanel.AutoSize = true;
			this.questionPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
			this.questionPanel.Location = new System.Drawing.Point(0, 0);
			this.questionPanel.Name = "questionPanel";
			this.questionPanel.Question = null;
			this.questionPanel.Score = 4;
			this.questionPanel.Sentance = "";
			this.questionPanel.Size = new System.Drawing.Size(873, 554);
			this.questionPanel.TabIndex = 2;
			// 
			// Viev
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
			this.ClientSize = new System.Drawing.Size(873, 554);
			this.Controls.Add(this.mainPanel);
			this.Controls.Add(this.questionPanel);
			this.Controls.Add(this.quizPanel);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Name = "Viev";
			this.Text = "Viev";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private HomePanel mainPanel;
		private QuizPanel quizPanel;
		private QuestionPanel questionPanel;
	}
}