﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using QuizLib;


namespace TestMaker.Viev
{
	public partial class Viev : Form, IViev
	{

		#region Properties
		public IHomePanel HomePanel
		{
			get => mainPanel;
			set => mainPanel = (HomePanel)value;
		}
		public IQuizPanel QuizPanel
		{
			get => quizPanel;
			set => quizPanel = (QuizPanel)value;
		}
		public IQuestionPanel QuestionPanel
		{
			get => questionPanel;
			set => questionPanel = (QuestionPanel)value;
		}
		#endregion



		#region EventHandling
		public event Func<string, Quiz> LoadQuiz;
		#endregion



		public void clearQuestionPanel()
		{
			questionPanel.Question = null;
		}

		public void loadQuiz(string path)
		{
			try
			{
				quizPanel.Editing = true;
				quizPanel.Quiz = LoadQuiz?.Invoke(path);
				questionPanel.ClearPanel();
				questionPanel.Question = LoadQuiz?.Invoke(path).Questions[0];
			}
			catch(Exception e)
			{
				questionPanel.ResetCounter();
				questionPanel.ClearPanel();
			}
		}

		public void createNewQuiz()
		{
			quizPanel.Editing = false;
		}

		public void goToQuizPanel()
		{
			mainPanel.Visible = false;
			questionPanel.Visible = false;
			quizPanel.Visible = true;
		}

		public void goToQuestionPanel()
		{
			mainPanel.Visible = false;
			quizPanel.Visible = false;
			questionPanel.Visible = true;
		}

		public void goToHomePanel()
		{
			mainPanel.Visible = true;
			quizPanel.Visible = false;
			questionPanel.Visible = false;
		}



		// Constructor
		public Viev()
		{
			InitializeComponent();
		}
	}
}
