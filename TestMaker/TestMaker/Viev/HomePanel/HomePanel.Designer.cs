﻿namespace TestMaker.Viev
{
	partial class HomePanel
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label2 = new System.Windows.Forms.Label();
			this.editQuizButton = new System.Windows.Forms.Button();
			this.newQuizButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.welcomeLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label2.Location = new System.Drawing.Point(206, 374);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(501, 50);
			this.label2.TabIndex = 9;
			this.label2.Text = "Edytuj wcześniej stworzony test.";
			// 
			// editQuizButton
			// 
			this.editQuizButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(68)))), ((int)(((byte)(99)))));
			this.editQuizButton.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
			this.editQuizButton.FlatAppearance.BorderSize = 0;
			this.editQuizButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.editQuizButton.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.editQuizButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.editQuizButton.Location = new System.Drawing.Point(22, 337);
			this.editQuizButton.Name = "editQuizButton";
			this.editQuizButton.Size = new System.Drawing.Size(163, 122);
			this.editQuizButton.TabIndex = 8;
			this.editQuizButton.Text = "Edytuj";
			this.editQuizButton.UseVisualStyleBackColor = false;
			this.editQuizButton.Click += new System.EventHandler(this.editQuizButton_Click);
			// 
			// newQuizButton
			// 
			this.newQuizButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(68)))), ((int)(((byte)(99)))));
			this.newQuizButton.Cursor = System.Windows.Forms.Cursors.Default;
			this.newQuizButton.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
			this.newQuizButton.FlatAppearance.BorderSize = 0;
			this.newQuizButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.newQuizButton.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.newQuizButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.newQuizButton.Location = new System.Drawing.Point(22, 209);
			this.newQuizButton.Name = "newQuizButton";
			this.newQuizButton.Size = new System.Drawing.Size(163, 122);
			this.newQuizButton.TabIndex = 7;
			this.newQuizButton.Text = "Nowy";
			this.newQuizButton.UseVisualStyleBackColor = false;
			this.newQuizButton.Click += new System.EventHandler(this.newQuizButton_Click);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe Print", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.label1.Location = new System.Drawing.Point(206, 233);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(622, 50);
			this.label1.TabIndex = 6;
			this.label1.Text = "Utwórz nowy test wielokrotnego wyboru";
			// 
			// welcomeLabel
			// 
			this.welcomeLabel.AutoSize = true;
			this.welcomeLabel.Font = new System.Drawing.Font("Segoe Print", 24.23762F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.welcomeLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.welcomeLabel.Location = new System.Drawing.Point(168, 68);
			this.welcomeLabel.Name = "welcomeLabel";
			this.welcomeLabel.Size = new System.Drawing.Size(472, 59);
			this.welcomeLabel.TabIndex = 5;
			this.welcomeLabel.Text = "Witam w edytorze testów";
			// 
			// HomePanel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(24)))), ((int)(((byte)(24)))));
			this.Controls.Add(this.label2);
			this.Controls.Add(this.editQuizButton);
			this.Controls.Add(this.newQuizButton);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.welcomeLabel);
			this.Name = "HomePanel";
			this.Size = new System.Drawing.Size(850, 513);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button editQuizButton;
		private System.Windows.Forms.Button newQuizButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label welcomeLabel;
	}
}
