﻿using System;
using System.Windows.Forms;
using System.IO;
using QuizLib;

namespace TestMaker.Viev
{
	public partial class HomePanel : UserControl, IHomePanel
	{
		const string JSON_OFD_FILTER = "JSON|*.json";
		readonly string CURRENT_DIRECTORY_FULL_PATH;
		#region Fields
		OpenFileDialog ofd = new OpenFileDialog();
		#endregion


		#region Properties
		#endregion


		#region EventHandling
		public event Action CreateNewQuiz;
		public event Action GoToQuizPanel;
		public event Action<string> LoadQuiz;

		private void newQuizButton_Click(object sender, EventArgs e)
		{
			CreateNewQuiz?.Invoke();
			GoToQuizPanel?.Invoke();
		}

		private void editQuizButton_Click(object sender, EventArgs e)
		{
			if (ofd.ShowDialog() == DialogResult.OK)
			{
				LoadQuiz?.Invoke(System.IO.Path.GetFullPath(ofd.FileName));

				GoToQuizPanel?.Invoke();
			}
		}
		#endregion




		// Constructor
		public HomePanel()
		{
			CURRENT_DIRECTORY_FULL_PATH = Path.GetFullPath(Directory.GetCurrentDirectory());


			ConfigureFileDialogForJSONFiles();
			InitializeComponent();
		}

		private void ConfigureFileDialogForJSONFiles()
		{
			ofd.InitialDirectory = CURRENT_DIRECTORY_FULL_PATH;
			ofd.Filter = JSON_OFD_FILTER;
		}
	}
}
