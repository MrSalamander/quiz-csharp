﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMaker.Viev
{
	public interface IHomePanel
	{
		#region Properties

		#endregion
		event Action CreateNewQuiz;
		event Action GoToQuizPanel;
		event Action<string> LoadQuiz;

	}
}
