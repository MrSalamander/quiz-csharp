﻿using System;
using System.Collections.Generic;
using QuizLib;

namespace TestMaker.Viev
{
	interface IViev
	{

		IHomePanel HomePanel { get; set; }
		IQuizPanel QuizPanel { get; set; }
		IQuestionPanel QuestionPanel { get; set; }


	}
}
