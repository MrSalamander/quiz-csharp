﻿using log4net;
using log4net.Config;
using System;
using System.Reflection;
using System.Windows.Forms;

namespace TestMaker
{
	static class Program
	{
		public static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			// MVP initialization
			Model.Model model = new Model.Model();
			Viev.Viev viev = new Viev.Viev();
			Presenter.Presenter presenter = new Presenter.Presenter(model, viev);
			Application.Run(viev);

			// configuration for logger
			XmlConfigurator.Configure();
			_log.Info("Aplication start \n");

		}
	}
}
