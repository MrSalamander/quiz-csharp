﻿using System;
using System.Collections.Generic;


namespace QuizLib
{
	[Serializable]
	public class Quiz
	{
		public Quiz(string title, string owner, string version, string descryption, uint duration, List<Double> wages)
		{

			Title = title;
			Owner = owner;
			Version = version;
			Descryption = descryption;
			Duration = duration;
			Wages = new List<Double>(wages);
			Questions = new List<Question>();
		}

	
		public string Title;
		public string Owner;
		public string Version;
		public uint Duration;
		public string Descryption;
		public List<Double> Wages;
		public List<Question> Questions;
	}

}
