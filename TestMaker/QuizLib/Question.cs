﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizLib
{
	[Serializable]
	public class Question
	{
		#region fields
		public string Sentance { get; set; }
		public int Score { get; set; }
		public List<Answer> Answers;
		#endregion
		public Question() { }
		public Question(string question, int score, List<Answer> answers)
		{
			this.Sentance = question;
			this.Score = score;
			this.Answers = new List<Answer>(answers);
		}
	}
}
