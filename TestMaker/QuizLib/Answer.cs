﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizLib
{
	[Serializable]
	public class Answer
	{
		#region fields
		public string text;
		public bool isTrue;
		#endregion

		public Answer() { }

		public Answer(string text, bool isTrue)
		{
			this.text = text;
			this.isTrue = isTrue;
		}
	}
}
